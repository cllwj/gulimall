package com.example.gulimall.ssoserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * @author chenlei
 * @date 2021-11-03 22:19
 * @description
 */
@Controller
public class LoginController {

    @Autowired
    RedisTemplate redisTemplate;

    @ResponseBody
    @GetMapping("/userInfo")
    public String getUserInfo(@RequestParam("token") String token){
        String userName = (String) redisTemplate.opsForValue().get(token);
        return userName;
    }

    @GetMapping("/login.html")
    public String loginPage(@RequestParam("redirect_url") String url, Model model,
                            @CookieValue(value = "sso_token",required = false) String sso_token){
        if(!StringUtils.isEmpty(sso_token)){
            return "redirect:" + url + "?token=" + sso_token;
        }
        model.addAttribute("url",url);
        return "login";
    }

    @PostMapping("/doLogin")
    public String doLogin(String username, String password, String url, HttpServletResponse response){
        if(!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)){
            // 登录成功,跳回之前页面
            String uuid = UUID.randomUUID().toString().replace("-","");
            redisTemplate.opsForValue().set(uuid,username);
            Cookie ssoToken = new Cookie("sso_token", uuid);
            // 返回给浏览器
            response.addCookie(ssoToken);
            return "redirect:" + url + "?token=" + uuid;
        }
        // 登录失败，显示登录页
        return "login";
    }
}
