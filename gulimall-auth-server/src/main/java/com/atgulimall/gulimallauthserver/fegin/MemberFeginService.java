package com.atgulimall.gulimallauthserver.fegin;

import com.atguigu.common.utils.R;
import com.atgulimall.gulimallauthserver.vo.SocialUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author chenlei
 * @date 2021-11-01 11:28
 * @description
 */
@FeignClient("gulimall-member")
public interface MemberFeginService {

    @PostMapping("/member/member/oauth2/login")
    R oauthLogin(@RequestBody SocialUser socialUser);
}
