package com.atgulimall.gulimallauthserver.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.utils.HttpUtils;
import com.atguigu.common.utils.R;
import com.atguigu.common.vo.MemberRespVo;
import com.atgulimall.gulimallauthserver.fegin.MemberFeginService;
import com.atgulimall.gulimallauthserver.vo.SocialUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @author chenlei
 * @date 2021-10-29 11:55
 * @description
 */
@Slf4j
@Controller
public class Oauth2Controller {

    @Autowired
    private MemberFeginService memberFeginService;

    /**
     * 处理社交登录请求
     * 优化：将常量部分放到配置文件中
     *
     * @param code
     * @param session
     * @return
     * @throws Exception
     */
    @GetMapping("/oauth2.0/weibo/success")
    public String weibo(@RequestParam("code") String code, HttpSession session) throws Exception {
        Map<String, String> map = new HashMap<>(5);
        map.put("client_id", "4021888232");
        map.put("client_secret", "1f01c11a1e791fcb5317f80198f5e72d");
        map.put("grant_type", "authorization_code");
        map.put("redirect_uri", "http://auth.gulimall.com/oauth2.0/weibo/success");
        map.put("code", code);
        HttpResponse response = null;
        try {
            /**
             * 通过得到的code获取accessToken
             */
            response = HttpUtils.doPost("https://api.weibo.com",
                    "/oauth2/access_token",
                    "post",
                    new HashMap<>(1),
                    new HashMap<>(1),
                    map);
        } catch (Exception e) {
            log.error("调用微博认证授权接口失败！");
        }
        if (response.getStatusLine().getStatusCode() == 200) {
            String json = EntityUtils.toString(response.getEntity());
            SocialUser socialUser = JSON.parseObject(json, SocialUser.class);
            // 知道当前是哪个社交用户
            // 1、当前用户如果是第一次进网站，自动注册进来（为当前社交用户生成一个会员信息账号，以后这个账号就对应指定的会员）
            // 登录或者注册社交用户
            R r = null;
            try {
                r = memberFeginService.oauthLogin(socialUser);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (r != null && r.getCode() == 0) {
                // 获取用户的登录信息
                MemberRespVo data = r.getData(new TypeReference<MemberRespVo>() {
                });
                //
                /**
                 * 1、第一次使用session:命令浏览器保存卡号。JSESSIONID这个cookie
                 * 以后浏览器访问哪个网站就会带上这个网站的cookie
                 * 子域之间：gulimall.com   auth.gulimall.com  order.gulimall.com
                 * 发卡的时候（指定域名为父域名），即使是子域系统发的卡，也能让父域直接使用
                 */
                // TODO: 1、使用JSON的方式序列化到redis中
                // TODO: 2、默认发的令牌的作用域是当前域，需要解决子域session共享的问题,放大作用域
                session.setAttribute("loginUser", data);
                log.info("登录成功，用户信息：{}", data.toString());
                return "redirect:http://gulimall.com";
            } else {
                return "redirect:http://auth.gulimall.com/login.html";
            }
        } else {
            return "redirect:http://auth.gulimall.com/login.html";
        }
    }
}
