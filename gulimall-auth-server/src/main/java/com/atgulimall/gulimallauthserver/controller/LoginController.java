package com.atgulimall.gulimallauthserver.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.HttpSessionRequiredException;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;

/**
 * @author chenlei
 * @date 2021-10-22 15:13
 * @description
 */
@Controller
public class LoginController {

    /*@GetMapping("/login.html")
    public String loginPage(){

        return "login";
    }

    @GetMapping("/reg.html")
    public String regPage(){

        return "reg";
    }*/

    @GetMapping("/login.html")
    public String loginPage(HttpSession session){
        Object loginUser = session.getAttribute("loginUser");
        if(loginUser == null){
            // 用户没有登录,则到登录页
            return "login";
        }else{
            return "redirect:http://gulimall.com";
        }

    }


}
