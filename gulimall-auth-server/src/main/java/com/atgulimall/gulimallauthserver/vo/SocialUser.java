package com.atgulimall.gulimallauthserver.vo;

import lombok.Data;

/**
 * @author chenlei
 * @date 2021-10-29 14:30
 * @description
 */
@Data
public class SocialUser {

    private String access_token;
    private String remind_in;
    private long expires_in;
    private String uid;
    private String isRealName;

}
