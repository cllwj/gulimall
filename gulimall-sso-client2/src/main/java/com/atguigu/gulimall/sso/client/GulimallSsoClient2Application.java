package com.atguigu.gulimall.sso.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GulimallSsoClient2Application {

    public static void main(String[] args) {
        SpringApplication.run(GulimallSsoClient2Application.class, args);
    }

}
