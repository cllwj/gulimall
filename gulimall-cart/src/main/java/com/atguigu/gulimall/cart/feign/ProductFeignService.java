package com.atguigu.gulimall.cart.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author chenlei
 * @date 2021-11-05 15:47
 * @description
 */
@FeignClient("gulimall-product")
public interface ProductFeignService {

    /**
     * 根据商品id查询商品详细信息
     * @param skuId
     * @return
     */
    @RequestMapping("/product/skuinfo/info/{skuId}")
    R getSkuInfo(@PathVariable("skuId") Long skuId);

    /**
     * 通过skuId获取销售属性
     * @param skuId
     * @return
     */
    @GetMapping("/product/skusaleattrvalue/stringlist/{skuId}")
    List<String> getSkuSaleAttrValues(@PathVariable("skuId") Long skuId);

    /**
     * 通过skuId获取商品最新价格
     * @param skuId
     * @return
     */
    @GetMapping("/product/skuinfo/getPrice/{skuId}")
    BigDecimal getPrice(@PathVariable("skuId") Long skuId);
}
