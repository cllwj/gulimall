package com.atguigu.gulimall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.cart.feign.ProductFeignService;
import com.atguigu.gulimall.cart.interceptor.CartInterceptor;
import com.atguigu.gulimall.cart.service.CartService;
import com.atguigu.gulimall.cart.vo.Cart;
import com.atguigu.gulimall.cart.vo.CartItem;
import com.atguigu.gulimall.cart.vo.SkuInfoVo;
import com.atguigu.gulimall.cart.vo.UserInfoTo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * @author chenlei
 * @date 2021-11-04 16:09
 * @description
 */
@Slf4j
@Service
public class CartServiceImpl implements CartService {

    @Autowired
    StringRedisTemplate redisTemplate;

    public final String CART_PREFIX = "gulimall:cart:";

    @Autowired
    ProductFeignService productFeignService;

    @Autowired
    ThreadPoolExecutor executor;


    /**
     * 使用多线程异步编排的方式处理
     *
     * @param skuId
     * @param num
     * @return
     */
    @Override
    public CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException {
        // 1、获取购物车
        BoundHashOperations<String, Object, Object> ops = getCartOPs();

        String res = (String) ops.get(skuId.toString());
        if (StringUtils.isBlank(res)) {
            // 2、商品添加到购物车
            CartItem cartItem = new CartItem();
            CompletableFuture<Void> getSkuInfoTask = CompletableFuture.runAsync(() -> {
                // 远程调用当前要添加的商品的详细信息
                R skuInfo = productFeignService.getSkuInfo(skuId);
                SkuInfoVo data = skuInfo.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                });

                cartItem.setCheck(true);
                cartItem.setCount(num);
                cartItem.setImage(data.getSkuDefaultImg());
                cartItem.setTitle(data.getSkuTitle());
                cartItem.setPrice(data.getPrice());
                cartItem.setSkuId(skuId);
            }, executor);

            // 3、通过skuId获取销售属性
            CompletableFuture<Void> getSkuSaleAttrValuesTask = CompletableFuture.runAsync(() -> {
                List<String> skuSaleAttrValues = productFeignService.getSkuSaleAttrValues(skuId);
                cartItem.setSkuAttr(skuSaleAttrValues);
            }, executor);

            // 这里是阻塞等待，等以上两个任务都执行完了才执行下面的代码
            CompletableFuture.allOf(getSkuInfoTask, getSkuSaleAttrValuesTask).get();

            String cartItemJson = JSON.toJSONString(cartItem);
            // 将商品信息存放到redis中
            ops.put(skuId.toString(), cartItemJson);
            return cartItem;
        } else {
            // 购物车有此商品，修改商品数量即可
            CartItem cartItem = JSON.parseObject(res, CartItem.class);
            cartItem.setCount(cartItem.getCount() + num);
            // 将修改后的商品信息再存到redis中
            ops.put(skuId.toString(), JSON.toJSONString(cartItem));
            return cartItem;
        }
    }

    /**
     * 通过商品ID查询购物车
     * @param skuId
     * @return
     */
    @Override
    public CartItem getCartBySkuId(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOPs = getCartOPs();
        String str = (String) cartOPs.get(skuId.toString());
        CartItem cartItem = JSON.parseObject(str, CartItem.class);
        return cartItem;
    }

    /**
     * 获取该用户整个购物车的信息
     * 需要判断当前用户是否登录
     * @return
     */
    @Override
    public Cart getCart() throws ExecutionException, InterruptedException {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        Cart cart = new Cart();
        if(userInfoTo.getUserId() != null){
            // 登录
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            String tempCartKey = CART_PREFIX + userInfoTo.getUserKey();
            // 查看临时购物车中是否有数据
            List<CartItem> cartItems = getCartItems(tempCartKey);
            // 如果存在数据，则将临时购物车中的数据存入到购物车中【合并】
            if(cartItems != null && !cartItems.isEmpty()){
                for (CartItem item : cartItems) {
                    addToCart(item.getSkuId(),item.getCount());
                }
                // 清空临时购物车
                clearCart(tempCartKey);
            }
            // 查询购物车中的购物项,获取登录后的购物车的数据【包含合并过来的临时购物车的数据，和登录后的购物车的数据】
            List<CartItem> items = getCartItems(cartKey);
            cart.setItems(items);
        }else{
            // 未登录
            String cartKey = CART_PREFIX + userInfoTo.getUserKey();
            // 获取临时购物车的所有购物项
            List<CartItem> cartItems = getCartItems(cartKey);
            cart.setItems(cartItems);
        }
        return cart;
    }

    /**
     * 获取购物车
     * @return
     */
    private BoundHashOperations<String,Object,Object> getCartOPs(){
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        String cartKey = "";
        if(userInfoTo.getUserId() != null){
            // 登录用户
            cartKey = CART_PREFIX + userInfoTo.getUserId();
        }else{
            // 临时用户
            cartKey = CART_PREFIX + userInfoTo.getUserKey();
        }
        BoundHashOperations<String, Object, Object> operations = redisTemplate.boundHashOps(cartKey);
        return operations;
    }

    /**
     * 通过cartKey从redis中获取商品购物项的信息
     * @param cartKey
     * @return
     */
    private List<CartItem> getCartItems(String cartKey){
        BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(cartKey);
        // 获取购物车里面的商品信息
        List<Object> values = hashOps.values();
        if(values != null && !values.isEmpty()){
            List<CartItem> collect = values.stream().map((item) -> {
                CartItem cartItem = JSON.parseObject(item.toString(), CartItem.class);
                return cartItem;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }

    /**
     * 清空临时购物车
     * @param cartKey
     */
    @Override
    public void clearCart(String cartKey) {
        redisTemplate.delete(cartKey);
    }


    /**
     * 修改购物项的选中状态
     * @param skuId
     * @param check
     */
    @Override
    public void checkItem(Long skuId, Integer check) {
        CartItem cartItem = getCartBySkuId(skuId);
        cartItem.setCheck(check == 1 ? true : false);
        BoundHashOperations<String, Object, Object> cartOPs = getCartOPs();
        String itemStr = JSON.toJSONString(cartItem);
        cartOPs.put(skuId.toString(),itemStr);
    }

    /**
     * 修改商品的数量
     * @param skuId
     * @param num
     */
    @Override
    public void changeItemCount(Long skuId, Integer num) {
        CartItem cartItem = getCartBySkuId(skuId);
        cartItem.setCount(num);
        BoundHashOperations<String, Object, Object> cartOPs = getCartOPs();
        String itemStr = JSON.toJSONString(cartItem);
        cartOPs.put(skuId.toString(),itemStr);
    }

    @Override
    public void deleteItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOPs = getCartOPs();
        cartOPs.delete(skuId.toString());
    }


    @Override
    public List<CartItem> getUserCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if(userInfoTo == null){
            return null;
        }else{
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            List<CartItem> cartItems = getCartItems(cartKey);
            // 获取所有被选中的购物项并且商品的价格是最新的，实时查询数据库
            List<CartItem> collect = cartItems.stream()
                    .filter(item -> item.getCheck())
                    .map(item -> {
                        // 更新为最新的价格
                        BigDecimal price = productFeignService.getPrice(item.getSkuId());
                        item.setPrice(price);
                        return item;
                    }).collect(Collectors.toList());
            log.info("返回结果：{}",collect.toString());
            return collect;
        }
    }
}
