package com.atguigu.gulimall.cart.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @author chenlei
 * @date 2021-11-04 16:19
 * @description
 */
@ToString
@Data
public class UserInfoTo {

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 临时键
     */
    private String userKey;

    /**
     * 临时用户
     */
    private boolean tempUser = false;
}
