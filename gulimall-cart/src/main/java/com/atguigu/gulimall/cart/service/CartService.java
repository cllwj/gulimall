package com.atguigu.gulimall.cart.service;

import com.atguigu.gulimall.cart.vo.Cart;
import com.atguigu.gulimall.cart.vo.CartItem;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author chenlei
 * @date 2021-11-04 16:08
 * @description
 */
public interface CartService {
    CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException;

    CartItem getCartBySkuId(Long skuId);

    Cart getCart() throws ExecutionException, InterruptedException;

    /**
     * 删除临时购物车的购物项
     * @param cartKey
     */
    void clearCart(String cartKey);

    /**
     * 修改购物项的选中状态
     * @param skuId
     * @param check
     */
    void checkItem(Long skuId, Integer check);

    /**
     * 修改购物项的数量
     * @param skuId
     * @param num
     */
    void changeItemCount(Long skuId, Integer num);

    /**
     * 删除购物车中的购物项
     * @param skuId
     */
    void deleteItem(Long skuId);

    /**
     * 查询购物车中被选中的购物项并且所有商品的价格是最新的
     * @return
     */
    List<CartItem> getUserCartItems();
}
