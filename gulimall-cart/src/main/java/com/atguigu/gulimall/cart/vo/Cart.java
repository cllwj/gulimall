package com.atguigu.gulimall.cart.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author chenlei
 * @date 2021-11-04 15:30
 * @description 需要计算的属性 必须重写它的get方法，保证每次获取属性都会进行计算
 */
public class Cart {

    private List<CartItem> items;

    /**
     * 商品数量
     */
    private Integer countNum;

    /**
     * 商品类型数量
     */
    private Integer counType;

    /**
     * 商品总价
     */
    private BigDecimal totalAmount;

    /**
     * 减免价格
     */
    private BigDecimal reduce = new BigDecimal("0.00");

    public List<CartItem> getItems() {
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }

    public Integer getCountNum() {
        int count = 0;
        if(items != null && !items.isEmpty()){
            for (CartItem cartItem : items){
                count += cartItem.getCount();
            }
        }
        return count;
    }

    public Integer getCounType() {
        int count = 0;
        if(items != null && !items.isEmpty()){
            for (CartItem cartItem : items){
                count += 1;
            }
        }
        return count;
    }

    /**
     * 只计算选中后的商品进行总价求和
     * @return
     */
    public BigDecimal getTotalAmount() {
        // 1、计算购物项总价
        BigDecimal amount = new BigDecimal("0");
        if(items != null && !items.isEmpty()){
            for (CartItem cartItem : items){
                if(cartItem.getCheck()){
                    BigDecimal totalPrice = cartItem.getTotalPrice();
                    amount = amount.add(totalPrice);
                }
            }
        }
        // 减去优惠总价
        BigDecimal subtract = amount.subtract(getReduce());
        return subtract;
    }

    public BigDecimal getReduce() {
        return reduce;
    }

    public void setReduce(BigDecimal reduce) {
        this.reduce = reduce;
    }

}
