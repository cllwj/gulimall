package com.atguigu.gulimall.member.service;

import com.atguigu.gulimall.member.vo.SocialUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.member.entity.MemberEntity;

import java.io.IOException;
import java.util.Map;

/**
 * 会员
 *
 * @author cl
 * @email sunlightcs@gmail.com
 * @date 2021-08-15 09:33:55
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    MemberEntity login(SocialUser socialUser);
}

