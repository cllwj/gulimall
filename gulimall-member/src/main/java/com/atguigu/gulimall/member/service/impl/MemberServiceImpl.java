package com.atguigu.gulimall.member.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atguigu.common.utils.HttpUtils;
import com.atguigu.gulimall.member.vo.SocialUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.member.dao.MemberDao;
import com.atguigu.gulimall.member.entity.MemberEntity;
import com.atguigu.gulimall.member.service.MemberService;

@Slf4j
@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }


    /**
     * 社交登录
     *
     * @param socialUser
     * @return
     */
    @Override
    public MemberEntity login(SocialUser socialUser) {
        // 登录注册合并逻辑
        // 获取用户uid
        String uid = socialUser.getUid();
        // 通过获取到的uid判断该用户是否在会员表中存在
        MemberEntity memberEntity = baseMapper.selectOne(new QueryWrapper<MemberEntity>().eq("social_uid", uid));
        if (memberEntity != null) {
            // 如果库里面有，则修改该用户的token信息及过期时间
            MemberEntity entity = new MemberEntity();
            entity.setAccessToken(socialUser.getAccess_token());
            entity.setExpiresIn(String.valueOf(socialUser.getExpires_in()));
            entity.setId(memberEntity.getId());
            // 通过主键id修改会员信息
            baseMapper.updateById(entity);
            memberEntity.setAccessToken(socialUser.getAccess_token());
            memberEntity.setExpiresIn(String.valueOf(socialUser.getExpires_in()));
            return memberEntity;
        } else {
            MemberEntity regEntity = new MemberEntity();
            // 注册用户信息到会员表
            // 通过用户的token及uid信息获取用户的昵称、性别等信息
            Map<String, String> queryMap = new HashMap<>(2);
            queryMap.put("access_token", socialUser.getAccess_token());
            queryMap.put("uid", socialUser.getUid());
            HttpResponse response = null;
            try {
                response = HttpUtils.doGet("https://api.weibo.com", "/2/users/show.json", "get", new HashMap<String, String>(), queryMap);
                if (response.getStatusLine().getStatusCode() == 200) {
                    // 查询成功
                    String json = EntityUtils.toString(response.getEntity());
                    JSONObject jsonObject = JSON.parseObject(json);
                    String name = jsonObject.getString("name");
                    String gender = jsonObject.getString("gender");
                    regEntity.setNickname(name);
                    regEntity.setGender("m".equals(gender) ? 1 : 0);
                }
            } catch (Exception e) {
                log.error("通过token获取用户信息调用接口失败！");
            }
            regEntity.setAccessToken(socialUser.getAccess_token());
            regEntity.setSocialUid(socialUser.getUid());
            regEntity.setExpiresIn(String.valueOf(socialUser.getExpires_in()));
            baseMapper.insert(regEntity);
            return regEntity;
        }
    }

}