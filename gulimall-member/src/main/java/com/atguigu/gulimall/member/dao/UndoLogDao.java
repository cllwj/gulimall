package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author cl
 * @email sunlightcs@gmail.com
 * @date 2021-08-15 09:33:55
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
