package product;


import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallProductApplication {

    /** 生成一个唯一的 Bucket 名称 */
    public static String generateUniqueBucketName(String prefix) {
        // 获取当前时间戳
        String timestamp = String.valueOf(System.currentTimeMillis());
        // 生成一个 0 到 9999 之间的随机数
        Random random = new Random();
        int randomNum = random.nextInt(10000); // 生成一个 0 到 9999 之间的随机数
        // 连接以形成一个唯一的 Bucket 名称
        return prefix + "-" + timestamp + "-" + randomNum;
    }

    @Test
    public void testUpload()throws FileNotFoundException {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint="oss-cn-beijing.aliyuncs.com";
        String accessKeyId ="LTAI5t8TNX3iW8CJYzuSzz5Z";
        String accessKeySecret="giEXFTfTbsATuiKaeE9dJKZMYHkTFL";
        OSS ossclient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        // 上传文件流。
        InputStream inputStream = new FileInputStream("C:\\Users\\32086\\Pictures\\Space02-Default.jpg");
        ossclient.putObject("gulimall-cll","test.jpg",inputStream);
        // 关闭osclient
        ossclient.shutdown();
    }
}
