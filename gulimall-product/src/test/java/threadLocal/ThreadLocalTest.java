package threadLocal;

/**
 * @author chenlei
 * @date 2022-02-16 10:43
 * @description
 */
public class ThreadLocalTest {
    private String content;

    private String getContent() {
        return content;
    }

    private void setContent(String content) {
        this.content = content;
    }

    /**
     * 使用synchronized加锁，导致程序运行的效率降低了，线程只能一个个排队执行代码
     * @param args
     */
    public static void main(String[] args) {
        ThreadLocalTest demo = new ThreadLocalTest();
        for (int i = 0; i < 5; i++) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized (ThreadLocalTest.class){
                        demo.setContent(Thread.currentThread().getName() + "的数据");
                        System.out.println("-----------------------");
                        System.out.println(Thread.currentThread().getName() + "--->" + demo.getContent());
                    }
                }
            });
            thread.setName("线程" + i);
            thread.start();
        }
    }
}
