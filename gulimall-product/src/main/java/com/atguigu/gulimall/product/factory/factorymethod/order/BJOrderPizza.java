package com.atguigu.gulimall.product.factory.factorymethod.order;

import com.atguigu.gulimall.product.factory.factorymethod.pizza.BJCheesePizza;
import com.atguigu.gulimall.product.factory.factorymethod.pizza.Pizza;

/**
 * @author chenlei
 * @date 2021-12-28 17:12
 * @description
 */
public class BJOrderPizza extends OrderPizza{
    @Override
    Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if("greek".equals(orderType)){
            pizza = new BJCheesePizza();
        }else if("cheese".equals(orderType)){
            pizza = new BJCheesePizza();
        }
        return pizza;
    }
}
