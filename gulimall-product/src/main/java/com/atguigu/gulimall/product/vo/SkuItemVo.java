package com.atguigu.gulimall.product.vo;

import com.atguigu.gulimall.product.entity.SkuImagesEntity;
import com.atguigu.gulimall.product.entity.SkuInfoEntity;
import com.atguigu.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-10-27 10:53
 * @description
 */
@Data
public class SkuItemVo {

    // 1.sku基本信息获取 pms_sku_info
    private SkuInfoEntity info;

    // 2、sku的图片信息 pms_spu_images
    private List<SkuImagesEntity> images;

    // 3、获取spu的销售属性组合
    private List<SkuItemSaleAttrVo> saleAttr;

    // 4、获取spu的介绍
    private SpuInfoDescEntity desc;

    // 5、获取spu的规格参数信息
    private List<SpuItemAttrGroupVo> groupAttrs;

    /**
     * 是否有库存
     */
    private boolean hasStock = true;
}
