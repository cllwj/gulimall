package com.atguigu.gulimall.product.factory.factorymethod.order;

import com.atguigu.gulimall.product.factory.factorymethod.pizza.Pizza;
import com.atguigu.gulimall.product.factory.pizza.CheesePizza;
import com.atguigu.gulimall.product.factory.pizza.GreekPizza;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author chenlei
 * @date 2021-12-28 17:13
 * @description
 */
@Slf4j
public abstract class OrderPizza {

    abstract Pizza createPizza(String orderType);

    public OrderPizza(){
        Pizza pizza = null;
        String orderType;
        do{
            orderType = getType();
            pizza = createPizza(orderType);
            // 制作过程
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
        }while (true);
    }

    private String getType(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        log.info("input pizza type:");
        try {
            String str = bufferedReader.readLine();
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
