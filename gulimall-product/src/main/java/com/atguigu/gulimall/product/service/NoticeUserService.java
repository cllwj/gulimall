//package com.atguigu.gulimall.product.service;
//
//import com.atguigu.gulimall.product.entity.MessageNoticeEntity;
//import com.atguigu.gulimall.product.entity.NoticeUserEntity;
//import com.atguigu.gulimall.product.vo.NoticeUserParam;
//import com.atguigu.gulimall.product.vo.NoticeUserResultVo;
//import com.atguigu.gulimall.product.vo.NoticeUserVo;
//import com.baomidou.mybatisplus.extension.service.IService;
//
//import java.util.Map;
//
///**
// *
// *
// * @author chenlei
// * @date 2021-10-11 10:37:38
// */
//public interface NoticeUserService extends IService<NoticeUserEntity> {
//
//    ResponseVo<NoticeUserVo> queryNoticeByUserId(NoticeUserParam noticeUserParam);
//
//    ResponseVo<MessageNoticeEntity> queryNoticeList(NoticeUserResultVo noticeUserResultVo);
//
//    Map<String,Object> queryNoticeById(Long noticeId,String userId);
//
//    boolean updatereadStatusByUserId(String userId);
//
//    ResponseVo<Map<String, Object>> queryNoticeByUserIdV2(String userId);
//
//    Integer queryNoReadNumNotice(String userId);
//}
//
