package com.atguigu.gulimall.product.factory.factorymethod.pizza;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenlei
 * @date 2021-12-28 17:11
 * @description
 */
@Slf4j
public class LDCheesePizza extends Pizza{
    @Override
    public void prepare() {
        setName("伦敦奶酪");
        log.info("给伦敦的奶酪披萨准备原材料...");
    }
}
