package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author chenlei
 * @date 2021-10-27 11:11
 * @description
 */
@Data
class AttrValueWithSkuIdVo {

    private String attrValue;

    private String skuIds;

}
