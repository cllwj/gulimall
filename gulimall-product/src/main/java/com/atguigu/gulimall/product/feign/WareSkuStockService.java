package com.atguigu.gulimall.product.feign;

import com.atguigu.common.to.SkuHasStockVo;
import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-09-12 11:41
 * @description
 */
@FeignClient("gulimall-ware")
public interface WareSkuStockService {

    @PostMapping("/ware/waresku/hassotck")
    R getSkuHasStock(@RequestBody List<Long> skuIds);
}
