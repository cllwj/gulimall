package com.atguigu.gulimall.product.factory.video;

/**
 * @author chenlei
 * @date 2021-12-29 11:13
 * @description
 */
public class Test {

    public static void main(String[] args) {
        Video javaVideo = VideoFactory.createVideo("java");
        javaVideo.product();

        Video py = VideoFactory.getVideo(PythonVideo.class);
        py.product();
    }
}
