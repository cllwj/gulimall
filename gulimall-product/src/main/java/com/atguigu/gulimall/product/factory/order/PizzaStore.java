package com.atguigu.gulimall.product.factory.order;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenlei
 * @date 2021-12-28 15:57
 * @description 相当于一个客户端 发出订购
 */
@Slf4j
public class PizzaStore {

    public static void main(String[] args) {
//        new OrderPizza();
        new OrderPizza(new SimpleFactory());
        log.info("订购结束...");
    }
}
