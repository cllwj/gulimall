package com.atguigu.gulimall.product.factory.factorymethod.pizza;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenlei
 * @date 2021-12-28 17:09
 * @description
 */
@Slf4j
public class BJCheesePizza extends Pizza{

    @Override
    public void prepare() {
        setName("北京奶酪");
        log.info("给北京的奶酪披萨准备原材料..");
    }
}
