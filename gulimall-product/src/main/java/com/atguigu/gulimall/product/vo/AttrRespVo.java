package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author chenlei
 * @date 2021-08-25 17:27
 * @description
 */
@Data
public class AttrRespVo extends AttrVo{

    /**
     * 所属分类
     */
    private String catelogName;


    /**
     * 所属分组
     */
    private String groupName;

    /**
     * 分类完整路径
     */
    private Long[] catelogPath;
}
