package com.atguigu.gulimall.product.factory.video;

/**
 * @author chenlei
 * @date 2021-12-29 11:09
 * @description
 */
public class VideoFactory {

    // 使用if-else方法
    public static Video createVideo(String type){
        Video video = null;
        if("java".equals(type)){
            video = new JavaVideo();
        }else if("python".equals(type)){
            video = new PythonVideo();
        }
        return video;
    }

    public static Video getVideo(Class c){
        Video video = null;
        try {
            video = (Video) Class.forName(c.getName()).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return video;
    }
}
