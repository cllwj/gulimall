package com.atguigu.gulimall.product.web;

import com.atguigu.gulimall.product.utils.PdfUtil;
import com.baomidou.mybatisplus.extension.api.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/pdf")
public class PdfController {

    @Autowired
    private TemplateEngine templateEngine;

    /**
     * pdf预览
     *
     * @param response HttpServletResponse
     */
    @GetMapping(value = "/preview")
    public void preview(HttpServletResponse response) {
        // 构造freemarker模板引擎参数,listVars.size()个数对应pdf页数
        List<Map<String,Object>> listVars = new ArrayList<>();
        Map<String,Object> variables = new HashMap<>(4);
        variables.put("title","测试预览PDF!");
        variables.put("imageUrl","http://localhost:8088/imgs/sg.jpg");
        List<Map<String,String>> demoList = new ArrayList<>();
        Map<String,String> demoMap = new HashMap<>(8);
        demoMap.put("name","哈哈");
        demoMap.put("nick","娃娃");
        demoMap.put("age","19");
        Map<String,String> demoMap2 = new HashMap<>(8);
        demoMap2.put("name","天天");
        demoMap2.put("nick","饭饭");
        demoMap2.put("age","14");
        demoList.add(demoMap);
        demoList.add(demoMap2);
        variables.put("demoList",demoList);
        Map<String,Object> variables2 = new HashMap<>(4);
        variables2.put("title","测试预览PDF2!");
        listVars.add(variables);
        listVars.add(variables2);

        PdfUtil.preview(templateEngine,"pdfPage",listVars,response);
    }

    /**
     * pdf下载
     *
     * @param response HttpServletResponse
     */
    @GetMapping(value = "/download")
    public void download(HttpServletResponse response) {
        List<Map<String,Object>> listVars = new ArrayList<>(1);
        Map<String,Object> variables = new HashMap<>(4);
        variables.put("title","测试下载PDF!");
        variables.put("imageUrl","https://localhost:10001/imgs/sg.jpg");
        List<Map<String,String>> demoList = new ArrayList<>();
        Map<String,String> demoMap = new HashMap<>(8);
        demoMap.put("name","哈哈");
        demoMap.put("nick","娃娃");
        demoMap.put("age","19");
        Map<String,String> demoMap2 = new HashMap<>(8);
        demoMap2.put("name","天天");
        demoMap2.put("nick","饭饭");
        demoMap2.put("age","14");
        demoList.add(demoMap);
        demoList.add(demoMap2);
        variables.put("demoList",demoList);
        listVars.add(variables);
        PdfUtil.download(templateEngine,"pdfPage",listVars,response,"测试打印.pdf");
    }

    /**
     * pdf下载到特定位置
     *
     */
    @GetMapping(value = "/pdf/save")
    public R<String> pdfSave() {
        List<Map<String,Object>> listVars = new ArrayList<>(1);
        Map<String,Object> variables = new HashMap<>(4);
        variables.put("title","测试下载PDF!");
        variables.put("imageUrl","http://localhost:8088/imgs/sg.jpg");
        List<Map<String,String>> demoList = new ArrayList<>();
        Map<String,String> demoMap = new HashMap<>(8);
        demoMap.put("name","哈哈");
        demoMap.put("nick","娃娃");
        demoMap.put("age","19");
        Map<String,String> demoMap2 = new HashMap<>(8);
        demoMap2.put("name","天天");
        demoMap2.put("nick","饭饭");
        demoMap2.put("age","14");
        demoList.add(demoMap);
        demoList.add(demoMap2);
        variables.put("demoList",demoList);
        listVars.add(variables);
        // pdf文件下载位置
        String pdfPath = "E:\\study\\code\\gulimall\\gulimall-product\\src\\main\\resources\\测试pdf.pdf" ;
        PdfUtil.save(templateEngine,"pdfPage",listVars,pdfPath);
        return R.ok("pdf保存成功");
    }

}
