package com.atguigu.gulimall.product.factory.absfactory.order;


import com.atguigu.gulimall.product.factory.absfactory.pizza.LDCheesePizza;
import com.atguigu.gulimall.product.factory.absfactory.pizza.LDPepperPizza;
import com.atguigu.gulimall.product.factory.absfactory.pizza.Pizza;

/**
 * @author chenlei
 * @date 2021-12-28 17:12
 * @description
 */
public class LDOrderPizza implements AbsFactory{

    @Override
     public Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if("pepper".equals(orderType)){
            pizza = new LDPepperPizza();
        }else if("cheese".equals(orderType)){
            pizza = new LDCheesePizza();
        }
        return pizza;
    }
}
