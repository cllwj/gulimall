package com.atguigu.gulimall.product.factory.absfactory.order;

/**
 * @author chenlei
 * @date 2021-12-28 17:20
 * @description
 */
public class PizzaStore {

    public static void main(String[] args) {
//        new OrderPizza(new BJOrderPizza());

        new OrderPizza(new LDOrderPizza());
    }
}
