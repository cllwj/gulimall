package com.atguigu.gulimall.product.factory.order;


import com.atguigu.gulimall.product.factory.pizza.CheesePizza;
import com.atguigu.gulimall.product.factory.pizza.GreekPizza;
import com.atguigu.gulimall.product.factory.pizza.Pizza;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author chenlei
 * @date 2021-12-28 15:38
 * @description 订购披萨
 */
@Slf4j
public class OrderPizza {

    /*public OrderPizza() {
        Pizza pizza = null;
        String orderType;
        do{
            orderType = getType();
            if("greek".equals(orderType)){
                pizza = new GreekPizza();
                pizza.setName("greekName");
            }else if("cheek".equals(orderType)){
                pizza = new CheesePizza();
                pizza.setName("cheekName");
            }else{
                break;
            }
            // 制作过程
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
        }while (true);
    }*/

    SimpleFactory simpleFactory;
    Pizza pizza = null;

    public OrderPizza(SimpleFactory simpleFactory) {
        setFactory(simpleFactory);
    }

    public void setFactory(SimpleFactory simpleFactory){
        this.simpleFactory = simpleFactory;
        String orderType = "";
        do{
            orderType = getType();
            pizza = this.simpleFactory.createPizza(orderType);
            if(pizza != null){
                pizza.prepare();
                pizza.bake();
                pizza.cut();
                pizza.box();
            }else{
                log.info("订购失败");
                break;
            }
        }while (true);
    }

    private String getType(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        log.info("input pizza type:");
        try {
            String str = bufferedReader.readLine();
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
