package com.atguigu.gulimall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-09-14 10:55
 * @description 2级分类VO
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Catelog2Vo {

    /**
     * 一级父分类ID
     */
    private String catalog1Id;
    /**
     * 3级子分类
     */
    private List<Catelog3Vo> catalog3List;

    private String id;
    private String name;

    /**
     * 三级分类vo
     */
    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Catelog3Vo{
        /**
         * 二级父分类ID
         */
        private String catalog2Id;
        private String id;
        private String name;
    }

}
