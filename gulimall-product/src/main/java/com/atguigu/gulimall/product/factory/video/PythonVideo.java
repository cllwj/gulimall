package com.atguigu.gulimall.product.factory.video;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenlei
 * @date 2021-12-29 11:09
 * @description
 */
@Slf4j
public class PythonVideo extends Video{
    @Override
    public void product() {
        log.info("python video...");
    }
}
