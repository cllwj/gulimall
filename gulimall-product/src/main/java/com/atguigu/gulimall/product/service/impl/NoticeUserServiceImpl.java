//package com.atguigu.gulimall.product.service.impl;
//
//import com.atguigu.gulimall.product.entity.MessageNoticeEntity;
//import com.atguigu.gulimall.product.entity.NoticeUserEntity;
//import com.atguigu.gulimall.product.service.MessageNoticeService;
//import com.atguigu.gulimall.product.service.NoticeUserService;
//import com.atguigu.gulimall.product.vo.NoticeUserParam;
//import com.atguigu.gulimall.product.vo.NoticeUserResultVo;
//import com.atguigu.gulimall.product.vo.NoticeUserVo;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@Slf4j
//@Service
//public class NoticeUserServiceImpl extends ServiceImpl<NoticeUserDao, NoticeUserEntity> implements NoticeUserService {
//
//    @Autowired
//    private MessageNoticeService messageNoticeService;
//
//    @Autowired
//    private NoticeUserDao noticeUserDao;
//
//    @Override
//    public ResponseVo<NoticeUserVo> queryNoticeByUserId(NoticeUserParam noticeUserParam) {
//        // 通过登录用户的ID查询关联表，获取对应的公告ID,并通过公告ID查询公告表，最终得到对应的公告
//        int current = noticeUserParam.getPageIndex();
//        String userId = noticeUserParam.getUserId();
//        QueryWrapper<NoticeUserEntity> wrapper = new QueryWrapper<>();
//        wrapper.eq("USER_ID",userId);
//        wrapper.eq("B_DELETE",0);
//        wrapper.eq("NOTICE_READ_STATUS", ConstantUtil.NOTICE_READ_STATUS_UNREAD);
//        wrapper.orderByDesc("CREATE_TIME");
//        List<NoticeUserEntity> noticeUserEntityList = baseMapper.selectList(wrapper);
//        NoticeUserEntity noticeUserEntity = null;
//        int unReadNum = 0;
//        if(noticeUserEntityList != null && !noticeUserEntityList.isEmpty()){
//            if(current >= noticeUserEntityList.size()){
//                noticeUserEntity = null;
//            }else{
//                noticeUserEntity = noticeUserEntityList.get(current);
//            }
//            unReadNum = noticeUserEntityList.size();
//        }
//
//
//        NoticeUserVo noticeUserVo = new NoticeUserVo();
//        noticeUserVo.setUnreadNum(unReadNum);
//        List<NoticeUserVo> noticeUserVos = new ArrayList<>();
//
//        if(noticeUserEntity != null && noticeUserEntity.getNoticeId() != null){
//            // 通过公告ID查询公告信息,未删除，已发布的信息
//            QueryWrapper<MessageNoticeEntity> queryWrapper = new QueryWrapper<>();
//            queryWrapper.eq("ID",noticeUserEntity.getNoticeId());
//            queryWrapper.eq("RELEASE_STATUS",ConstantUtil.RELEASE_STATUS_PUBLISHED);
//            queryWrapper.eq("B_DELETE",0);
//            MessageNoticeEntity messageNoticeEntity = messageNoticeService.getOne(queryWrapper);
//            BeanUtils.copyProperties(messageNoticeEntity,noticeUserVo);
//            noticeUserVo.setNoticeId(noticeUserEntity.getNoticeId().toString());
//        }
//        noticeUserVos.add(noticeUserVo);
//
//        Long size = Long.valueOf(noticeUserEntityList.size());
//        if(noticeUserVos.get(0).getId() == null){
//            noticeUserVos = null;
//        }
//        return new ResponseVo<>(noticeUserVos,size);
//    }
//
//    @Override
//    public ResponseVo<Map<String, Object>> queryNoticeByUserIdV2(String userId) {
//        // 这个接口只调用一次，一次把该用户的所有未读的数据返回，该接口不计算未读的数量
//        List<Map<String, Object>> maps = noticeUserDao.queryNoticeUserEntity(userId);
//        for(Map<String, Object> map : maps){
//            Long id = (Long) map.get("id");
//            map.put("noticeId",id.toString());
//        }
//        Long size = Long.valueOf(maps.size());
//        return new ResponseVo<>(maps,size);
//    }
//
//    @Override
//    public Integer queryNoReadNumNotice(String userId) {
//        QueryWrapper<NoticeUserEntity> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("USER_ID",userId);
//        queryWrapper.eq("NOTICE_READ_STATUS",ConstantUtil.NOTICE_READ_STATUS_UNREAD);
//        queryWrapper.eq("B_DELETE",0);
//        Integer noReadNum = baseMapper.selectCount(queryWrapper);
//        return noReadNum;
//    }
//
//    /**
//     * 查询用户公告信息（0=未读 1=已读 2=全部）
//     * @param noticeUserResultVo
//     * @return
//     */
//    @Override
//    public ResponseVo<MessageNoticeEntity> queryNoticeList(NoticeUserResultVo noticeUserResultVo) {
//        String userId = noticeUserResultVo.getUserId();
//        int readStatus = noticeUserResultVo.getReadStatus();
//        QueryWrapper<NoticeUserEntity> wrapper = new QueryWrapper<>();
//        wrapper.eq("USER_ID",userId);
//        wrapper.eq("B_DELETE",0);
//
//        // 未读
//        if(readStatus == ConstantUtil.NOTICE_READ_STATUS_UNREAD){
//            wrapper.eq("NOTICE_READ_STATUS",ConstantUtil.NOTICE_READ_STATUS_UNREAD);
//        }
//
//        // 已读
//        if(readStatus == ConstantUtil.NOTICE_READ_STATUS_READ){
//            wrapper.eq("NOTICE_READ_STATUS",ConstantUtil.NOTICE_READ_STATUS_READ);
//        }
//        List<NoticeUserEntity> noticeUserEntityList = baseMapper.selectList(wrapper);
//        // 收集公告ID
//        List<String> noticeIds = new ArrayList<>();
//        for(NoticeUserEntity noticeUserEntity : noticeUserEntityList){
//            noticeIds.add(noticeUserEntity.getNoticeId().toString());
//        }
//
//        int current = noticeUserResultVo.getPageIndex();
//        int limit = noticeUserResultVo.getPageSize();
//        // 通过noticeIds查询公告信息
//        return messageNoticeService.getNoticeByIds(userId,noticeIds, current, limit);
//    }
//
//    /**
//     * 点击查看详情 通过公告ID查询公告信息，并将该条公告修改为已读
//     * @param noticeId
//     * @return
//     */
//    @Override
//    public Map<String,Object> queryNoticeById(Long noticeId,String userId) {
//        MessageNoticeEntity messageNoticeEntity = messageNoticeService.getById(noticeId);
//        // 通过userId和noticeId修改该公告为已读
//        NoticeUserEntity noticeUserEntity = new NoticeUserEntity();
//        noticeUserEntity.setNoticeReadStatus(ConstantUtil.NOTICE_READ_STATUS_READ);
//        UpdateWrapper<NoticeUserEntity> updateWrapper = new UpdateWrapper<>();
//        updateWrapper.eq("USER_ID",userId);
//        updateWrapper.eq("NOTICE_ID",noticeId);
//        baseMapper.update(noticeUserEntity,updateWrapper);
//
//        // 修改后。再获取该用户未读的条数
//        QueryWrapper<NoticeUserEntity> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("USER_ID",userId);
//        queryWrapper.eq("NOTICE_READ_STATUS",ConstantUtil.NOTICE_READ_STATUS_UNREAD);
//        Integer noReadNum = baseMapper.selectCount(queryWrapper);
//
//        Map<String,Object> map = new HashMap<>();
//        map.put("messageNoticeEntity",messageNoticeEntity);
//        map.put("noReadNum",noReadNum);
//        return map;
//    }
//
//    @Override
//    public boolean updatereadStatusByUserId(String userId) {
//        NoticeUserEntity noticeUserEntity = new NoticeUserEntity();
//        noticeUserEntity.setNoticeReadStatus(ConstantUtil.NOTICE_READ_STATUS_READ);
//        UpdateWrapper<NoticeUserEntity> updateWrapper = new UpdateWrapper<>();
//        updateWrapper.eq("USER_ID",userId);
//        baseMapper.update(noticeUserEntity,updateWrapper);
//        return true;
//    }
//}