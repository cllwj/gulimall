package com.atguigu.gulimall.product.factory.absfactory.order;

import com.atguigu.gulimall.product.factory.absfactory.pizza.Pizza;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author chenlei
 * @date 2021-12-28 17:13
 * @description
 */
@Slf4j
public class OrderPizza {

    public OrderPizza(AbsFactory absFactory){
        setFactory(absFactory);
    }

    private void setFactory(AbsFactory absFactory){
        Pizza pizza = null;
        String orderType = "";
        do{
            orderType = getType();
            pizza = absFactory.createPizza(orderType);
            if(pizza != null){
                // 制作过程
                pizza.prepare();
                pizza.bake();
                pizza.cut();
                pizza.box();
            }else{
                log.info("订购失败");
                break;
            }
        }while (true);

    }

    private String getType(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        log.info("input pizza type:");
        try {
            String str = bufferedReader.readLine();
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
