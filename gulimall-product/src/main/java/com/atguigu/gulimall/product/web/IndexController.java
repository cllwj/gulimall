package com.atguigu.gulimall.product.web;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.Catelog2Vo;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author chenlei
 * @date 2021-09-14 10:01
 * @description
 */
@Slf4j
@Controller
public class IndexController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RedissonClient redisson;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @GetMapping({"/","/index.html"})
    public String indexPage(Model model){
        // 查询一级分类
       List<CategoryEntity> categoryEntityList = categoryService.getLevel1Categorys();
        model.addAttribute("categorys",categoryEntityList);
        // 前缀默认：classPath:/template/
        // 后缀默认：.html
        return "index";
    }

    @ResponseBody
    @GetMapping("index/catalog.json")
    public Map<String, List<Catelog2Vo>> getCatelogJson(){
        Map<String, List<Catelog2Vo>> map = categoryService.getCategorys();
        return map;
    }

    @ResponseBody
    @GetMapping("/hello")
    public String redissonTest(){
        // 获取同一把锁
        RLock lock = redisson.getLock("MY-LOCK");
        /**
         * 加锁，阻塞式等待
         * 1）锁的自动续期，如果业务执行时间超长，运行期间自动给锁续上新的30s,不用担心业务执行时间过长，锁自动过期被删掉
         * 2）加锁的业务只要运行完成，就不会给当前锁续期，即使不手动解锁，也会自动释放锁
         *    redisson不会出现死锁的情况
         */
//        lock.lock();
        // 在加锁的时候，设置锁的过期时间，10秒钟自动解锁，锁到时间以后不会自动续期。
        /**
         * 1、如果传递了锁的超时时间，就发送给redis执行脚本，进行占锁，默认超时就是我们指定的时间
         * 2、如果未指定锁的超时时间，就使用30 * 10000【看门狗默认时间】，只要占锁成功，就会启动定时任务【重新给锁设置过期时间，新的过期时间就是看门狗的默认时间】，每隔10秒就会续期一次
         */
        lock.lock(10, TimeUnit.SECONDS);
        try {
            log.info("加锁成功，执行业务逻辑...." + Thread.currentThread().getId());
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // 解锁，假设解锁代码没有运行，redisson会不会出现死锁的问题
            log.info("释放锁成功...." + Thread.currentThread().getId());
            lock.unlock();
        }
        return "hello";
    }

    @GetMapping("write")
    @ResponseBody
    public String writeValue(){
        // 获取读写锁
        RReadWriteLock readWriteLock = redisson.getReadWriteLock("rw-lock");
        String s = "";
        // 获取写锁
        RLock rLock = readWriteLock.writeLock();
        try {
            // 1、改数据加写锁，读取数据加读锁
            rLock.lock();
            s = UUID.randomUUID().toString();
            Thread.sleep(30000);
            redisTemplate.opsForValue().set("writeValue",s);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            rLock.unlock();
        }
        return s;
    }

    @GetMapping("read")
    @ResponseBody
    public String readValue(){
        RReadWriteLock readWriteLock = redisson.getReadWriteLock("rw-lock");
        String s = "";
        // 1、添加读锁
        RLock rLock = readWriteLock.readLock();
        rLock.lock();
        try {

            s = redisTemplate.opsForValue().get("writeValue");
            Thread.sleep(30000);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rLock.unlock();
        }
        return s;
    }

    /**
     * 车库停车
     * 3车位
     * 信号量也可以额用作分布式限流
     * @return
     */
    @GetMapping("park")
    @ResponseBody
    public String park(){
        RSemaphore park = redisson.getSemaphore("park");
        // 获取一个信号，获取一个值，这里是占用一个车位
        boolean b = park.tryAcquire();
        return "ok = >" + b;
    }

    @GetMapping("go")
    @ResponseBody
    public String go(){
        RSemaphore park = redisson.getSemaphore("park");
        // 释放一个车位
        park.release();
        return "ok!";
    }


}
