//package com.atguigu.gulimall.product.entity;
//
//import com.baomidou.mybatisplus.annotation.FieldFill;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import lombok.Data;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// *
// *
// * @author chenlei
// * @date 2021-10-11 10:37:38
// */
//@Data
//@TableName("SYS_NOTICE_USER")
//public class NoticeUserEntity implements Serializable {
//	private static final long serialVersionUID = 1L;
//
//	/**
//	 *
//	 */
//	@TableId
//	private Long id;
//	/**
//	 * 公告ID
//	 */
//	private Long noticeId;
//	/**
//	 * 用户ID
//	 */
//	private Long userId;
//	/**
//	 * 公告读取状态(0=未读 1=已读)
//	 */
//	private Integer noticeReadStatus;
//
//	/**
//	 * 是否删除（0=未删除  1=已删除）
//	 */
//	private Integer bDelete;
//
//	/**
//	 * 创建时间
//	 */
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//	@TableField(fill = FieldFill.INSERT)
//	private Date createTime;
//
//}
