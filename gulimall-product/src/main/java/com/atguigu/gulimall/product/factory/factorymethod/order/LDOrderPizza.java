package com.atguigu.gulimall.product.factory.factorymethod.order;

import com.atguigu.gulimall.product.factory.factorymethod.pizza.LDCheesePizza;
import com.atguigu.gulimall.product.factory.factorymethod.pizza.Pizza;

/**
 * @author chenlei
 * @date 2021-12-28 17:12
 * @description
 */
public class LDOrderPizza extends OrderPizza{

    @Override
    Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if("greek".equals(orderType)){
            pizza = new LDCheesePizza();
        }else if("cheese".equals(orderType)){
            pizza = new LDCheesePizza();
        }
        return pizza;
    }
}
