package com.atguigu.gulimall.product.factory.factorymethod.pizza;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenlei
 * @date 2021-12-28 17:10
 * @description
 */
@Slf4j
public class BJPepperPizza extends Pizza{
    @Override
    public void prepare() {
        setName("北京胡椒");
        log.info("给北京的胡椒pisa准备原材料...");
    }
}
