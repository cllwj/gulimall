//package com.atguigu.gulimall.product.vo;
//
//import com.alibaba.csp.sentinel.util.StringUtil;
//import com.atguigu.gulimall.product.entity.MessageNoticeEntity;
//import lombok.Data;
//
///**
// * @author chenlei
// * @date 2021-10-12 14:37
// * @description
// */
//@Data
//public class NoticeUserResultVo extends MessageNoticeEntity {
//
//    private String _pageSize="10";
//
//    private String _pageIndex="0";
//
//    public Integer getPageSize(){
//        return StringUtil.isEmpty(_pageSize)?10:Integer.valueOf(_pageSize);
//    }
//
//    public Integer getPageIndex(){
//        return StringUtil.isEmpty(_pageIndex)?0:Integer.valueOf(_pageIndex);
//    }
//
//    /**
//     * 用户ID
//     */
//    private String userId;
//
//    /**
//     * 读取状态 (0=未读 1=已读 2=全部）
//     */
//    private int readStatus;
//
//}
