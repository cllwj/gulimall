//package com.atguigu.gulimall.product.entity;
//
//import com.baomidou.mybatisplus.annotation.FieldFill;
//import com.baomidou.mybatisplus.annotation.TableField;
//import com.baomidou.mybatisplus.annotation.TableId;
//import com.baomidou.mybatisplus.annotation.TableName;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import lombok.Data;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// *
// *
// * @author chenlei
// * @date 2021-10-11 10:37:37
// */
//@Data
//@TableName("SYS_MESSAGE_NOTICE")
//public class MessageNoticeEntity implements Serializable {
//	private static final long serialVersionUID = 1L;
//
//	/**
//	 * 主键
//	 */
//	@TableId
//	private Long id;
//
//	/**
//	 * 通知标题
//	 */
//	private String title;
//
//	/**
//	 * 通知内容
//	 */
//	private String content;
//
//	/**
//	 * 创建时间
//	 */
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//	@TableField(fill = FieldFill.INSERT)
//	private Date createTime;
//
//	/**
//	 * 修改时间
//	 */
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//	@TableField(fill = FieldFill.INSERT_UPDATE)
//	private Date updateTime;
//
//	/**
//	 * 发布类型（0=立即发送  1=定时发送 ）
//	 */
//	private Integer releaseType;
//
//	/**
//	 * 发布状态（0=待发布 1=已发布）
//	 */
//	private Integer releaseStatus;
//
//	/**
//	 * 发布时间
//	 */
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
//	private Date releaseTime;
//
//	/**
//	 * 是否删除（0=未删除  1=已删除）
//	 */
//	private Integer bDelete;
//
//
//	/**
//	 * 上传文件id
//	 */
//	private String fileId;
//
//	/**
//	 * 附件数量
//	 */
//	@TableField(exist = false)
//	private int fileNum;
//
//	@TableField(exist = false)
//	private String noticeId;
//
//	/**
//	 * 读取状态 (0=未读 1=已读 2=全部）
//	 */
//	@TableField(exist = false)
//	private int readStatus;
//
//}
