//package com.atguigu.gulimall.product.app;
//
//import com.atguigu.gulimall.product.entity.MessageNoticeEntity;
//import com.atguigu.gulimall.product.entity.NoticeUserEntity;
//import com.atguigu.gulimall.product.service.MessageNoticeService;
//import com.atguigu.gulimall.product.service.NoticeUserService;
//import com.atguigu.gulimall.product.vo.MessageNoticeVo;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.util.HtmlUtils;
//
//import java.util.Date;
//import java.util.List;
//
///**
// * @author chenlei
// * @date 2021-10-11 10:37:37
// * @description 后台管理端
// */
//@Slf4j
//@RestController
//@RequestMapping("system/notice")
//public class MessageNoticeController {
//
//    @Autowired
//    private MessageNoticeService messageNoticeService;
//
//    @Autowired
//    private NoticeUserService noticeUserService;
//
//    /**
//     * 列表
//     */
//    @PostMapping("/list")
//    public Response<ResponseVo<MessageNoticeEntity>> list(@RequestBody MessageNoticeVo messageNoticeVo){
//        return Response.genResult(messageNoticeService.queryPage(messageNoticeVo));
//    }
//
//    /**
//     * 信息
//     */
//    @GetMapping("/info/{id}")
//    public Response<MessageNoticeEntity> info(@PathVariable("id") Long id){
//        MessageNoticeEntity messageNotice = messageNoticeService.getById(id);
//        return Response.genResult(messageNotice);
//    }
//
//    /**
//     * 保存
//     */
//    @PostMapping("/save")
//    public Response<String> save(@RequestBody MessageNoticeEntity messageNotice) {
//        if(messageNotice.getContent() != null
//                && messageNotice.getContent().length() > ConstantUtil.NATICE_CONTENT_LENGTH){
//            return Response.genError("超过公告内容最大长度限制！");
//        }
//        messageNotice.setContent(HtmlUtils.htmlUnescape(messageNotice.getContent()));
//        if(messageNoticeService.saveNotice(messageNotice)){
//            return Response.genResult("保存成功！");
//        }
//        return Response.genError("保存失败！");
//    }
//
//    /**
//     * 修改 已发布的公告不能修改，只有未发布的公告才能修改
//     */
//    @PostMapping("/update")
//    public Response<String> update(@RequestBody MessageNoticeEntity messageNotice){
//        // 通过id查询公告发布状态
//        String noticeId = messageNotice.getNoticeId();
//        Long id = Long.valueOf(noticeId);
//        messageNotice.setId(id);
//        MessageNoticeEntity noticeInfo = messageNoticeService.getById(id);
//        Integer releaseStatus = noticeInfo.getReleaseStatus();
//
//        if(releaseStatus == ConstantUtil.RELEASE_STATUS_UNPUBLISHED){
//            // 判断传入的时间是否为空，若不为空，则比较传入的时间和当前时间进行比较，如果小于当前时间，则立即发布
//            Date releaseTime = messageNotice.getReleaseTime();
//            if(releaseTime != null && DateUtils.contrastTime(releaseTime)){
//                // 立即发布,将发布时间改为当前时间
//                messageNotice.setReleaseTime(new Date());
//                List<NoticeUserEntity> noticeUserEntityList = messageNoticeService.getNoticeUser(messageNotice);
//                boolean flag = false;
//                if(noticeUserEntityList != null && !noticeUserEntityList.isEmpty()){
//                    flag = noticeUserService.saveBatch(noticeUserEntityList);
//                }
//                if(flag){
//                    // 立即发布成功后，将公告表中的发布状态改为已发布
//                    messageNotice.setReleaseStatus(ConstantUtil.RELEASE_STATUS_PUBLISHED);
////                    messageNoticeService.update(messageNotice,new UpdateWrapper<MessageNoticeEntity>().eq("id",messageNotice.getId()));
//                    log.info("修改定时发布时间小于当前时间，立即发布成功！");
//                }
//            }
//            if(messageNoticeService.updateById(messageNotice)){
//                return Response.genResult("修改成功！");
//            }
//        }else{
//            return Response.genResult("该公告已发布，不可修改！");
//        }
//        return Response.genResult("修改失败！");
//    }
//
//    /**
//     * 删除
//     */
//    @PostMapping("/delete")
//    public Response<String> delete(String noticeId){
//        Long id = Long.valueOf(noticeId);
//        if(messageNoticeService.removeNotice(id)){
//            return Response.genResult("删除成功");
//        }
//        return Response.genResult("删除失败");
//    }
//
//}
