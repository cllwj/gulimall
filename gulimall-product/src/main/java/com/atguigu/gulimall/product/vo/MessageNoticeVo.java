//package com.atguigu.gulimall.product.vo;
//
//import com.alibaba.csp.sentinel.util.StringUtil;
//import com.atguigu.gulimall.product.entity.MessageNoticeEntity;
//import com.fasterxml.jackson.annotation.JsonFormat;
//import lombok.Data;
//
//import java.io.Serializable;
//import java.util.Date;
//
///**
// * @author chenlei
// * @date 2021-10-11 11:43
// * @description
// */
//@Data
//public class MessageNoticeVo extends MessageNoticeEntity implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//
//    private String _pageSize="10";
//
//    private String _pageIndex="0";
//
//    public Integer getPageSize(){
//        return StringUtil.isEmpty(_pageSize)?10:Integer.valueOf(_pageSize);
//    }
//
//    public Integer getPageIndex(){
//        return StringUtil.isEmpty(_pageIndex)?0:Integer.valueOf(_pageIndex);
//    }
//
//    private String noticeId;
//
//    /**
//     * 开始时间
//     */
//    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
//    private Date startDate;
//
//    /**
//     * 结束时间
//     */
//    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
//    private Date endDate;
//}
