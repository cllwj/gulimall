package com.atguigu.gulimall.product.factory.calendar;

import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;

/**
 * @author chenlei
 * @date 2021-12-29 10:23
 * @description
 */
@Slf4j
public class Factory {
    public static void main(String[] args) {
        //
        Calendar cal = Calendar.getInstance();
        log.info("年：{}",cal.get(Calendar.YEAR));
        log.info("月：{}",cal.get(Calendar.MONTH) + 1);
        log.info("日：{}",cal.get(Calendar.DAY_OF_MONTH));


    }
}
