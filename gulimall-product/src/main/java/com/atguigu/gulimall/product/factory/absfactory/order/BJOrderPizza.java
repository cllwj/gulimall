package com.atguigu.gulimall.product.factory.absfactory.order;

import com.atguigu.gulimall.product.factory.absfactory.pizza.BJCheesePizza;
import com.atguigu.gulimall.product.factory.absfactory.pizza.BJPepperPizza;
import com.atguigu.gulimall.product.factory.absfactory.pizza.Pizza;

/**
 * @author chenlei
 * @date 2021-12-28 17:12
 * @description
 */
public class BJOrderPizza implements AbsFactory {
    @Override
    public Pizza createPizza(String orderType) {
        System.out.println("使用的是抽象工厂方法~~~");
        Pizza pizza = null;
        if("pepper".equals(orderType)){
            pizza = new BJPepperPizza();
        }else if("cheese".equals(orderType)){
            pizza = new BJCheesePizza();
        }
        return pizza;
    }
}
