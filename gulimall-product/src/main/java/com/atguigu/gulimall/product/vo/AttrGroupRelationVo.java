package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author chenlei
 * @date 2021-08-26 17:09
 * @description
 */
@Data
public class AttrGroupRelationVo {
    private Long attrId;
    private Long attrGroupId;
}
