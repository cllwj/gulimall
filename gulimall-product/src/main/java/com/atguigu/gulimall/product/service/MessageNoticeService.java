//package com.atguigu.gulimall.product.service;
//
//import com.atguigu.gulimall.product.entity.MessageNoticeEntity;
//import com.atguigu.gulimall.product.entity.NoticeUserEntity;
//import com.atguigu.gulimall.product.vo.MessageNoticeVo;
//import com.baomidou.mybatisplus.extension.service.IService;
//
//import java.util.List;
//
///**
// *
// *
// * @author chenlei
// * @email xxx@gmail.com
// * @date 2021-10-11 10:37:37
// */
//public interface MessageNoticeService extends IService<MessageNoticeEntity> {
//
//    ResponseVo<MessageNoticeEntity> queryPage(MessageNoticeVo messageNoticeVo);
//
//    boolean saveNotice(MessageNoticeEntity messageNotice);
//
//    boolean removeNotice(Long id);
//
//    ResponseVo<MessageNoticeEntity> getNoticeByIds(String userId,List<String> noticeIds,int current,int size);
//
//    List<NoticeUserEntity> getNoticeUser(MessageNoticeEntity noticeEntities);
//}
//
