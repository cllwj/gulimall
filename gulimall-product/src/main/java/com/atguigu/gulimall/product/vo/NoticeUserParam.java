//package com.atguigu.gulimall.product.vo;
//
//import com.alibaba.csp.sentinel.util.StringUtil;
//import lombok.Data;
//
///**
// * @author chenlei
// * @date 2021-10-15 16:57
// * @description
// */
//@Data
//public class NoticeUserParam extends NoticeUserResultVo {
//
//    private String userId;
//
//    private String _pageSize="10";
//
//    private String _pageIndex="0";
//
//    public Integer getPageSize(){
//        return StringUtil.isEmpty(_pageSize)?10:Integer.valueOf(_pageSize);
//    }
//
//    public Integer getPageIndex(){
//        return StringUtil.isEmpty(_pageIndex)?0:Integer.valueOf(_pageIndex);
//    }
//}
