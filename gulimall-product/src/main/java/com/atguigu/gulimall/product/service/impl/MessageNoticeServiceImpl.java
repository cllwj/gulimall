//package com.atguigu.gulimall.product.service.impl;
//
//import com.atguigu.gulimall.product.entity.MessageNoticeEntity;
//import com.atguigu.gulimall.product.entity.NoticeUserEntity;
//import com.atguigu.gulimall.product.service.MessageNoticeService;
//import com.atguigu.gulimall.product.service.NoticeUserService;
//import com.atguigu.gulimall.product.vo.MessageNoticeVo;
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//
//@Slf4j
//@Service
//public class MessageNoticeServiceImpl extends ServiceImpl<MessageNoticeDao, MessageNoticeEntity> implements MessageNoticeService {
//
//    @Autowired
//    private UserService userService;
//
//    @Autowired
//    private NoticeUserService noticeUserService;
//
//
//    @Override
//    public ResponseVo<MessageNoticeEntity> queryPage(MessageNoticeVo messageNoticeVo) {
//        Page<MessageNoticeEntity> page = new Page<>(messageNoticeVo.getPageIndex() + 1, messageNoticeVo.getPageSize());
//        QueryWrapper<MessageNoticeEntity> wrapper = new QueryWrapper<>();
//        wrapper.eq("B_DELETE",0);
//        if(StringUtils.isNotBlank(messageNoticeVo.getTitle())){
//            wrapper.like("TITLE",messageNoticeVo.getTitle());
//        }
//
//        if(messageNoticeVo.getReleaseStatus() != null){
//            wrapper.eq("RELEASE_STATUS",messageNoticeVo.getReleaseStatus());
//        }
//
//        if(messageNoticeVo.getStartDate() != null && messageNoticeVo.getEndDate() != null){
//            wrapper.ge("RELEASE_TIME",messageNoticeVo.getStartDate());
//            wrapper.le("RELEASE_TIME",messageNoticeVo.getEndDate());
//        }
//
//        wrapper.orderByDesc("CREATE_TIME");
//
//        baseMapper.selectPage(page, wrapper);
//        List<MessageNoticeEntity> records = page.getRecords();
//        for(MessageNoticeEntity messageNoticeEntity : records){
//            String fileId = messageNoticeEntity.getFileId();
//            messageNoticeEntity.setNoticeId(messageNoticeEntity.getId().toString());
//            if(StringUtils.isBlank(fileId)){
//                messageNoticeEntity.setFileNum(0);
//            }else{
//                String str[] = fileId.split(",");
//                messageNoticeEntity.setFileNum(Arrays.asList(str).size());
//            }
//        }
//        return new ResponseVo<>(records,page.getTotal());
//    }
//
//    /**
//     * 保存公告信息，并上传附件信息，保存成功后，通过判断发布类型 ，如果为立即发送，则发布公告，原子操作；
//     * 如果是定时发送，执行定时发送的逻辑
//     * @param messageNotice
//     * @return
//     */
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public boolean saveNotice(MessageNoticeEntity messageNotice) {
//        if(messageNotice == null){
//            log.error("输入参数为空！");
//            throw new BusinessException("输入参数为空！");
//        }
//
//        if(StringUtils.isBlank(messageNotice.getTitle())){
//            log.error("公告标题不能为空！");
//            throw new BusinessException("公告标题不能为空！");
//        }
//
//        if(StringUtils.isBlank(messageNotice.getContent())){
//            log.error("公告内容不能为空！");
//            throw new BusinessException("公告内容不能为空！");
//        }
//
//        if(messageNotice.getReleaseType() == null){
//            log.error("公告发布类型不能为空！");
//            throw new BusinessException("公告发布类型不能为空！");
//        }
//
//        // 如果传入的发布时间为空，则是立即发布，发布时间设置为当前时间
//        if(messageNotice.getReleaseTime() == null){
//            messageNotice.setReleaseTime(new Date());
//        }
//
//        if(messageNotice.getReleaseType() == ConstantUtil.RELEASE_TYPE_FIXED_TIME
//                && messageNotice.getReleaseTime() != null) {
//            Date date = new Date();
//            int i = date.compareTo(messageNotice.getReleaseTime());
//            // 如果当前时间大于定时发布时间，则将发布时间设置为当前时间
//            if (i == 1) {
//                messageNotice.setReleaseTime(new Date());
//            }
//        }
//
//        // 保存公告基本信息
//        baseMapper.insert(messageNotice);
//        List<NoticeUserEntity> noticeUserEntityList = getNoticeUser(messageNotice);
//
//        // 立即发送，则直接绑定用户id和公告id
//        if(messageNotice.getReleaseType() == ConstantUtil.RELEASE_TYPE_PUBLISH_NOW){
//            boolean saveFlag = false;
//            // 批量保存
//            if(noticeUserEntityList != null && !noticeUserEntityList.isEmpty()){
//                saveFlag = noticeUserService.saveBatch(noticeUserEntityList);
//            }
//
//            if(saveFlag){
//                // 立即发布成功后，将公告表中的发布状态改为已发布
//                messageNotice.setReleaseStatus(ConstantUtil.RELEASE_STATUS_PUBLISHED);
//                baseMapper.update(messageNotice,new UpdateWrapper<MessageNoticeEntity>().eq("id",messageNotice.getId()));
//                log.info("修改发布状态为已发布，并绑定公告信息与用户成功！");
//                return true;
//            }
//        }else if (messageNotice.getReleaseType() == ConstantUtil.RELEASE_TYPE_FIXED_TIME
//                      && messageNotice.getReleaseTime() != null){
//            Date date = new Date();
//            int i = date.compareTo(messageNotice.getReleaseTime());
//            // 如果当前时间大于定时发布时间，则立即发布
//            if(i == 1){
//                // 批量保存
//                boolean flag = false;
//                if(noticeUserEntityList != null && !noticeUserEntityList.isEmpty()){
//                    flag = noticeUserService.saveBatch(noticeUserEntityList);
//                }
//                if(flag){
//                    // 立即发布成功后，将公告表中的发布状态改为已发布
//                    messageNotice.setReleaseStatus(ConstantUtil.RELEASE_STATUS_PUBLISHED);
//                    baseMapper.update(messageNotice,new UpdateWrapper<MessageNoticeEntity>().eq("id",messageNotice.getId()));
//                    log.info("定时发送时间已过时改为立即发布，并绑定公告信息与用户成功！");
//                    return true;
//                }
//            }
//            return true;
//        }
//        return false;
//    }
//
//
//    /**
//     * 执行定时发送,通过公告的发布时间来定时发送公告，即定时保存公告和用户绑定关系
//     * 定时发送，通过当前时间和发布的时间进行比较，每一分钟执行一次
//     */
//    @Scheduled(cron = "0 0/1 * * * ?")
//    public void sendNoticeByFixedTime() {
//        // 获取当前时间
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date date = new Date();
//        String newFormat = sdf.format(date);
//        QueryWrapper<MessageNoticeEntity> wrapper = new QueryWrapper<>();
//        wrapper.eq("B_DELETE",0);
//        wrapper.eq("RELEASE_TYPE", ConstantUtil.RELEASE_TYPE_FIXED_TIME);
//        wrapper.eq("RELEASE_STATUS",ConstantUtil.RELEASE_STATUS_UNPUBLISHED);
//        wrapper.ge("RELEASE_TIME",newFormat);
//        List<MessageNoticeEntity> noticeEntities = baseMapper.selectList(wrapper);
//        for(MessageNoticeEntity messageNotice : noticeEntities){
//            String releaseFormat = sdf.format(messageNotice.getReleaseTime());
//            if (newFormat.equals(releaseFormat)) {
//                log.info("执行定时发布公告！");
//                List<NoticeUserEntity> noticeUser = getNoticeUser(messageNotice);
//                if (noticeUserService.saveBatch(noticeUser)) {
//                    // 发布后，修改发布状态为已发布
//                    messageNotice.setReleaseStatus(ConstantUtil.RELEASE_STATUS_PUBLISHED);
//                    baseMapper.update(messageNotice,new UpdateWrapper<MessageNoticeEntity>().eq("id",messageNotice.getId()));
//                    log.info("执行定时发布公告完成！");
//                }
//            }
//        }
//
//    }
//
//    /**
//     * 组装绑定用户ID和公告ID
//     * @param noticeEntities
//     * @return
//     */
//    @Override
//    public List<NoticeUserEntity> getNoticeUser(MessageNoticeEntity noticeEntities) {
//        List<NoticeUserEntity> noticeUserEntityList = new ArrayList<>();
//        // 获取所有用户信息
//        List<Long> userIds = userService.queryUserIds();
//        if (userIds != null && !userIds.isEmpty()) {
//            for (Long userId : userIds) {
//                NoticeUserEntity noticeUserEntity = new NoticeUserEntity();
//                noticeUserEntity.setNoticeId(noticeEntities.getId());
//                noticeUserEntity.setUserId(userId);
//                noticeUserEntityList.add(noticeUserEntity);
//            }
//        }
//        return noticeUserEntityList;
//    }
//
//    /**
//     * 逻辑删除，删除完成后用户端和后台管理端对应的的该通知均无法查看，定时发送的通知不会执行定时发送逻辑
//     * @param id
//     * @return
//     */
//    @Transactional(rollbackFor = Exception.class)
//    @Override
//    public boolean removeNotice(Long id) {
//        // 将公告表中的对应公告的B_DELETE字段设置为1
//        MessageNoticeEntity messageNoticeEntity = new MessageNoticeEntity();
//        messageNoticeEntity.setBDelete(1);
//        baseMapper.update(messageNoticeEntity,new UpdateWrapper<MessageNoticeEntity>().eq("id",id));
//
//        // 并将关系表中对应数据的B_DELETE字段设置为1
//        NoticeUserEntity noticeUserEntity = new NoticeUserEntity();
//        noticeUserEntity.setBDelete(1);
//        noticeUserService.update(noticeUserEntity,new UpdateWrapper<NoticeUserEntity>().eq("NOTICE_ID",id));
//        return true;
//    }
//
//    /**
//     * 通过公告ID查询公告信息
//     * @param noticeIds
//     * @return
//     */
//    @Override
//    public ResponseVo<MessageNoticeEntity> getNoticeByIds( String userId,List<String> noticeIds,int current,int size) {
//        Page<MessageNoticeEntity> page = new Page<>(current + 1,size);
//        QueryWrapper<MessageNoticeEntity> wrapper = new QueryWrapper<>();
//        wrapper.eq("B_DELETE",0);
//        wrapper.orderByDesc("CREATE_TIME");
//        if(noticeIds != null && !noticeIds.isEmpty()){
//            wrapper.in("ID",noticeIds);
//            baseMapper.selectPage(page, wrapper);
//        }
//        List<MessageNoticeEntity> records = page.getRecords();
//        for(MessageNoticeEntity messageNoticeEntity : records){
//            messageNoticeEntity.setNoticeId(messageNoticeEntity.getId().toString());
//            // 通过userId和noticeId查询SYS_NOTICE_USER表，获取该条消息的读取状态
//            QueryWrapper<NoticeUserEntity> noticeUserEntityQueryWrapper = new QueryWrapper<>();
//            noticeUserEntityQueryWrapper.eq("USER_ID",userId);
//            noticeUserEntityQueryWrapper.eq("B_DELETE",0);
//            noticeUserEntityQueryWrapper.eq("NOTICE_ID",messageNoticeEntity.getId());
//            NoticeUserEntity userEntity = noticeUserService.getOne(noticeUserEntityQueryWrapper);
//            messageNoticeEntity.setReadStatus(userEntity.getNoticeReadStatus());
//        }
//        return new ResponseVo<>(page.getRecords(),page.getTotal());
//    }
//
//}