package com.atguigu.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.atguigu.gulimall.product.vo.Catelog2Vo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        List<CategoryEntity> entities = baseMapper.selectList(null);
        // 查询一级目录
        List<CategoryEntity> collect = entities.stream().
                filter(category ->
            category.getParentCid() == 0
        ).map((item) -> {
            item.setChildren(getChildrens(item, entities));
            return item;
        }).sorted((menu1,menu2) ->{
           return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());

        return collect;
    }

    /**
     * 递归查询所有菜单的子菜单
     * @param root
     * @param all
     * @return
     */
    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> all){
        List<CategoryEntity> collect = all.stream().filter(category ->
                category.getParentCid() == root.getCatId()
        ).map((item) -> {
            item.setChildren(getChildrens(item, all));
            return item;
        }).sorted((menu1,menu2) ->{
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());

        return collect;
    }

    @Override
    public void removeMenusById(List<Long> asList) {
        // TODO 1、检查当前删除的菜单，是否被别的地方引用
        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] findPathBycatelogId(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        List<Long> parentPath = getParentPathById(catelogId, paths);
        Collections.reverse(parentPath);
        return parentPath.toArray(new Long[parentPath.size()]);
    }

//    @CacheEvict(value = "category",key = "'getLevel1Categorys'")

    /**
     * @CacheEvict: 失效模式
     * @param category
     */
    @CacheEvict(value = "category",allEntries = true)
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateByIdDetail(CategoryEntity category) {
        this.updateById(category);
        if(StringUtils.isNotBlank(category.getName())){
            categoryBrandRelationService.updateCategoryNameById(category.getCatId(),category.getName());
        }
    }

    /**
     * 每个需要缓存的数据我们都来指定要放到那个名字的缓存【缓存分区（按照业务类型分）】
     * @Cacheable:代表当前方法的结果需要缓存，如果缓存中有，方法不用调用，如果缓存中没有，会调用方法，最后将方法进行缓存
     * @return
     */
    @Cacheable(value = {"category"},key = "#root.method.name")
    @Override
    public List<CategoryEntity> getLevel1Categorys() {
        long startTime = System.currentTimeMillis();
        List<CategoryEntity> entities = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("cat_level", 1));
        log.info("查询一级菜单消耗的时间：{}" + (System.currentTimeMillis() - startTime));
        return entities;
    }

    @Cacheable(value = {"category"},key = "#root.method.name")
    @Override
    public Map<String, List<Catelog2Vo>> getCategorys() {
        List<CategoryEntity> categoryEntityList = baseMapper.selectList(null);
        log.info("查询了数据库.....");

        //查询出一级分类
        List<CategoryEntity> level1Categorys = getParent_cid(categoryEntityList,0L);
        // 封装数据
        Map<String, List<Catelog2Vo>> collect = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            // 1、每一个的一级分类，查到这个一级分类的二级分类
            List<CategoryEntity> catLevel2 = getParent_cid(categoryEntityList,v.getCatId());
            List<Catelog2Vo> catelog2 = null;
            if (catLevel2 != null && !catLevel2.isEmpty()) {
                catelog2 = catLevel2.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    // 2、查询当前二级分类的三级分类封装成vo
                    List<CategoryEntity> catLevel3 = getParent_cid(categoryEntityList,l2.getCatId());
                    if(catLevel3 != null && !catLevel3.isEmpty()){
                        List<Catelog2Vo.Catelog3Vo> catelog3Vos = catLevel3.stream().map(l3 -> {
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(catelog3Vos);
                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2;
        }));
        String s = JSONObject.toJSONString(collect);
        // 设置过期时间为1天
        redisTemplate.opsForValue().set("categoryJson",s,1, TimeUnit.DAYS);
        return collect;
    }

    /**
     * 查询分类数据，添加redis缓存
     * @return
     */
//    @Override
    public Map<String, List<Catelog2Vo>> getCategorys2() {

        /**
         * 1、空结果缓存，解决缓存穿透
         * 2、设置过期时间（加随机值）：解决缓存雪崩
         * 3、加锁：解决缓存击穿
         */
        // 给缓存中放json字符串，取出的json字符串，还用逆转为能用的对象类型 【序列化与反序列化】
        // 1、加入缓存逻辑，缓存中存放的数据是json字符串
        // JSON是跨平台，跨语言的
        String categoryJson = redisTemplate.opsForValue().get("categoryJson");
        if(StringUtils.isBlank(categoryJson)){
            // 缓存中没有，查询数据库
            log.info("缓存没有命中，调用getCategorysFromDb方法.............");
            Map<String, List<Catelog2Vo>> categorysFromDb = getCategorysFromDbWithRedisLock();
            return categorysFromDb;
        }
        log.info("缓存命中.........");
        Map<String, List<Catelog2Vo>> result = JSON.parseObject(categoryJson, new TypeReference<Map<String, List<Catelog2Vo>>>() {
        });

        return result;
    }

    /**
     * 使用redis实现分布式锁
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCategorysFromDbWithRedisLock() {
        String uuid = UUID.randomUUID().toString();
        /**
         * 在占坑的同时，设置过期时间，是为了防止死锁的问题，添加过期时间和加锁必须是原子性的
         */
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("lock", uuid,300,TimeUnit.SECONDS);
        if(lock){
            log.info("获取分布式锁成功.....");
            // 加锁成功......执行业务
            /**
             * 如果在此处设置锁的过期时间，当程序在占坑的时候出现异常，或者突然断电的极端情况，
             * 程序无法执行到此处，则该锁永远不会过期，就会出现死锁的情况
             * **********所有添加过期时间和加锁的过程必须是原子性的*********
             */
            // redisTemplate.expire("lock",30,TimeUnit.SECONDS);
            Map<String, List<Catelog2Vo>> cateoryData;
            try {
                cateoryData = getCateoryDataFromDb();
                /**
                 * 删除自己的锁
                 * 获取值对比+ 对比成功删除=原子操作
                 */
                /*String lockValue = redisTemplate.opsForValue().get("uuid");
                if(uuid.equals(lockValue)){
                    redisTemplate.delete("lock");
                }*/
            } finally {
                /**
                 * 原子解锁
                 */
                String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
                Long lock1 = redisTemplate.execute(new DefaultRedisScript<>(script, Long.class), Arrays.asList("lock"), uuid);
            }
            return cateoryData;
        }else{
            // 加锁失败.......   自旋重试
            log.info("获取分布式锁失败，等待重试...");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getCategorysFromDbWithRedisLock();
        }
    }

    /**
     * 使用Redisson实现分布式锁
     * 缓存中的数据如何和数据库保持一致
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCategorysFromDbWithRedissonLock() {
        // 1、占分布式锁，去redis占坑,锁的名字必须区分
        RLock lock = redissonClient.getLock("catalogJson-lock");
        lock.lock();
        log.info("获取分布式锁成功.....");

        Map<String, List<Catelog2Vo>> cateoryData;
        try {
            cateoryData = getCateoryDataFromDb();
        } finally {
            lock.unlock();
        }
        return cateoryData;
    }


    private Map<String, List<Catelog2Vo>> getCateoryDataFromDb() {
        String categoryJson = redisTemplate.opsForValue().get("categoryJson");
        if(!StringUtils.isEmpty(categoryJson)){
            Map<String, List<Catelog2Vo>> result = JSON.parseObject(categoryJson, new TypeReference<Map<String, List<Catelog2Vo>>>() {
            });
            return result;
        }

        List<CategoryEntity> categoryEntityList = baseMapper.selectList(null);
        log.info("查询了数据库.....");

        //查询出一级分类
        List<CategoryEntity> level1Categorys = getParent_cid(categoryEntityList,0L);
        // 封装数据
        Map<String, List<Catelog2Vo>> collect = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            // 1、每一个的一级分类，查到这个一级分类的二级分类
            List<CategoryEntity> catLevel2 = getParent_cid(categoryEntityList,v.getCatId());
            List<Catelog2Vo> catelog2 = null;
            if (catLevel2 != null && !catLevel2.isEmpty()) {
                catelog2 = catLevel2.stream().map(l2 -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                    // 2、查询当前二级分类的三级分类封装成vo
                    List<CategoryEntity> catLevel3 = getParent_cid(categoryEntityList,l2.getCatId());
                    if(catLevel3 != null && !catLevel3.isEmpty()){
                        List<Catelog2Vo.Catelog3Vo> catelog3Vos = catLevel3.stream().map(l3 -> {
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3Vo;
                        }).collect(Collectors.toList());
                        catelog2Vo.setCatalog3List(catelog3Vos);
                    }
                    return catelog2Vo;
                }).collect(Collectors.toList());
            }
            return catelog2;
        }));
        String s = JSONObject.toJSONString(collect);
        // 设置过期时间为1天
        redisTemplate.opsForValue().set("categoryJson",s,1, TimeUnit.DAYS);
        return collect;
    }

    /**
     * 从数据库查询并封装分类数据,添加本地锁
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCategorysFromDbWithLocalLock() {

        // TODO:本地锁：synchronized,JUC(Lock) ,在分布式情况下，想要锁住所有，必须使用分布式锁

        synchronized (this) {
            // 得到锁以后，应该再到缓存中确定一次，如果没有才需要继续查询
            String categoryJson = redisTemplate.opsForValue().get("categoryJson");
            if (!StringUtils.isEmpty(categoryJson)) {
                Map<String, List<Catelog2Vo>> result = JSON.parseObject(categoryJson, new TypeReference<Map<String, List<Catelog2Vo>>>() {
                });
                return result;
            }
            log.info("查询数据库....");
            /**
             * 将数据库的多次查询变为一次
             */
            List<CategoryEntity> categoryEntityList = baseMapper.selectList(null);

            //查询出一级分类
            List<CategoryEntity> level1Categorys = getParent_cid(categoryEntityList, 0L);
            // 封装数据
            Map<String, List<Catelog2Vo>> collect = level1Categorys.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
                // 1、每一个的一级分类，查到这个一级分类的二级分类
                List<CategoryEntity> catLevel2 = getParent_cid(categoryEntityList, v.getCatId());
                List<Catelog2Vo> catelog2 = null;
                if (catLevel2 != null && !catLevel2.isEmpty()) {
                    catelog2 = catLevel2.stream().map(l2 -> {
                        Catelog2Vo catelog2Vo = new Catelog2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());
                        // 2、查询当前二级分类的三级分类封装成vo
                        List<CategoryEntity> catLevel3 = getParent_cid(categoryEntityList, l2.getCatId());
                        if (catLevel3 != null && !catLevel3.isEmpty()) {
                            List<Catelog2Vo.Catelog3Vo> catelog3Vos = catLevel3.stream().map(l3 -> {
                                Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                                return catelog3Vo;
                            }).collect(Collectors.toList());
                            catelog2Vo.setCatalog3List(catelog3Vos);
                        }
                        return catelog2Vo;
                    }).collect(Collectors.toList());
                }
                return catelog2;
            }));
            String catelogStr = JSONObject.toJSONString(collect);
            // 设置过期时间为1天
            redisTemplate.opsForValue().set("categoryJson", catelogStr, 1, TimeUnit.DAYS);
            return collect;
        }
    }

    private List<CategoryEntity> getParent_cid(List<CategoryEntity> categoryEntityList, Long parent_cid) {
        List<CategoryEntity> collect = categoryEntityList.stream()
                .filter(item -> item.getParentCid() == parent_cid)
                .collect(Collectors.toList());
        return collect;
    }


    /**
     * 通过子ID查询父级ID
     * @param catelogId
     * @param paths
     * @return
     */
    private List<Long> getParentPathById(Long catelogId,List<Long> paths){
        paths.add(catelogId);
        CategoryEntity categoryEntity = this.getById(catelogId);
        if(categoryEntity != null && categoryEntity.getParentCid() != 0){
            getParentPathById(categoryEntity.getParentCid(),paths);
        }
        return paths;
    }


}