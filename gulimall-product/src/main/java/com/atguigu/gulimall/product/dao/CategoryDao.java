package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author cl
 * @email sunlightcs@gmail.com
 * @date 2021-08-16 17:02:58
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
