//package com.atguigu.gulimall.product.vo;
//
//import com.zhkj.jtb.platform.system.model.MessageNoticeEntity;
//import lombok.Data;
//
///**
// * @author chenlei
// * @date 2021-10-12 14:37
// * @description
// */
//@Data
//public class NoticeUserVo extends MessageNoticeEntity {
//
//    /**
//     * 未读条数
//     */
//    private int unreadNum;
//
//    private String noticeId;
//
//}
