package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author cl
 * @email sunlightcs@gmail.com
 * @date 2021-08-15 00:23:35
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
