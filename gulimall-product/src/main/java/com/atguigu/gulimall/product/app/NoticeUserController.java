//package com.atguigu.gulimall.product.app;
//
//import com.atguigu.gulimall.product.entity.MessageNoticeEntity;
//import com.atguigu.gulimall.product.service.NoticeUserService;
//import com.atguigu.gulimall.product.vo.NoticeUserParam;
//import com.atguigu.gulimall.product.vo.NoticeUserResultVo;
//import com.atguigu.gulimall.product.vo.NoticeUserVo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.Map;
//
///**
// * @author chenlei
// * @date 2021-10-12 14:19
// * @description 用户端
// */
//@RestController
//@RequestMapping("system/receiveNotice")
//public class NoticeUserController {
//
//    @Autowired
//    private NoticeUserService noticeUserService;
//
//    /**
//     * 用户登录后判断该用户对应的通知信息中是否存在未读信息
//     */
//    @PostMapping("/queryNoticeByUserId")
//    public Response<ResponseVo<NoticeUserVo>> queryNoticeByUserId(@RequestBody NoticeUserParam noticeUserParam){
//        // 获取登录用户的ID
//        String userId = AuthHelper.getUserId();
////        String userId = "540478105620736";
//        noticeUserParam.setUserId(userId);
//        return Response.genResult(noticeUserService.queryNoticeByUserId(noticeUserParam));
//    }
//
//    /**
//     * 用户查询公告信息
//     * @param noticeUserResultVo
//     * @return
//     */
//    @PostMapping("/queryNoticeList")
//    public Response<ResponseVo<MessageNoticeEntity>> queryNoticeList(@RequestBody NoticeUserResultVo noticeUserResultVo){
//         String userId = AuthHelper.getUserId();
////        String userId = "540478105620736";
//        noticeUserResultVo.setUserId(userId);
//        return Response.genResult(noticeUserService.queryNoticeList(noticeUserResultVo));
//    }
//
//
//    /**
//     * 点击查看详情 通过公告ID查询公告信息，并将该条公告修改为已读
//     * @param noticeId
//     * @return
//     */
//    @GetMapping("/queryNoticeById/{noticeId}")
//    public Response<Map<String,Object>> queryNoticeById(@PathVariable String noticeId){
//         String userId = AuthHelper.getUserId();
////        String userId = "540478105620736";
//        Long id = Long.valueOf(noticeId);
//        return Response.genResult(noticeUserService.queryNoticeById(id,userId));
//    }
//
//    /**
//     * 通过userId设置全部标记为已读
//     */
//    @GetMapping("/updateReadStatusByUserId")
//    public Response<String> updateReadStatusByUserId(){
//        String userId = AuthHelper.getUserId();
////        String userId = "540478105620736";
//        if(noticeUserService.updatereadStatusByUserId(userId)){
//            return Response.genResult("全部标记为已读成功！");
//        }else{
//            return Response.genResult("全部标记为已读失败！");
//        }
//    }
//
//    /**
//     * 查询该用户下未读的信息，前端只调用一次即可
//     * @return
//     */
//    @GetMapping("/queryNoReadNoticeInfo")
//    public Response<ResponseVo<Map<String, Object>>> queryNoReadNoticeInfo(){
//        String userId = AuthHelper.getUserId();
////        String userId = "540478105620736";
//        return Response.genResult(noticeUserService.queryNoticeByUserIdV2(userId));
//    }
//
//    /**
//     * 查询
//     * @return
//     */
//    @GetMapping("/queryNoReadNumNotice")
//    public Response<Integer> queryNoReadNumNotice(){
//        String userId = AuthHelper.getUserId();
////        String userId = "540478105620736";
//        Integer noReadMun = noticeUserService.queryNoReadNumNotice(userId);
//        return Response.genResult(noReadMun);
//    }
//
//}
