package com.atguigu.gulimall.product.factory.video;

/**
 * @author chenlei
 * @date 2021-12-29 11:08
 * @description
 */
public abstract class Video {

    public abstract void product();
}
