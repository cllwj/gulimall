package com.atguigu.gulimall.product.factory.absfactory.pizza;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenlei
 * @date 2021-12-28 17:11
 * @description
 */
@Slf4j
public class LDPepperPizza extends Pizza {
    @Override
    public void prepare() {
        setName("伦敦胡椒");
        log.info("给伦敦的胡椒披萨准备原材料...");
    }
}
