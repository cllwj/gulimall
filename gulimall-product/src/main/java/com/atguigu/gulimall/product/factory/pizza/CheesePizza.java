package com.atguigu.gulimall.product.factory.pizza;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenlei
 * @date 2021-12-28 15:35
 * @description
 */
@Slf4j
public class CheesePizza extends Pizza{

    @Override
    public void prepare() {
        log.info("给奶酪披萨准备原材料...");
    }
}
