package com.atguigu.gulimall.product.factory.absfactory.order;

import com.atguigu.gulimall.product.factory.absfactory.pizza.Pizza;

/**
 * @author chenlei
 * @date 2021-12-28 17:59
 * @description
 */
public interface AbsFactory {

    Pizza createPizza(String orderType);
}
