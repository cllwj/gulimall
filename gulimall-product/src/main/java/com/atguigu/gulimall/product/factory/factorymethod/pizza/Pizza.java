package com.atguigu.gulimall.product.factory.factorymethod.pizza;

import lombok.extern.slf4j.Slf4j;

/** 将Pizza类做成抽象的
 * @author chenlei
 * @date 2021-12-28 15:28
 * @description
 */
@Slf4j
public abstract class Pizza {

    /**
     * 披萨的名称
     */
    protected String name;

    /**
     * 准备原材料，不同的披萨的原材料不一样，所以做成抽象的
     */
    public abstract void prepare();

    /**
     * 烘焙
     */
    public void bake(){
       log.info(name + " bakeing");
    }

    /**
     * 切割
     */
    public void cut(){
        log.info(name + " cutting");
    }

    /**
     * 打包
     */
    public void box(){
        log.info(name + " boxing");
    }

    public void setName(String name) {
        this.name = name;
    }
}
