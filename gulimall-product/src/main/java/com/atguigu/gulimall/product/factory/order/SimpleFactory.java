package com.atguigu.gulimall.product.factory.order;

import com.atguigu.gulimall.product.factory.pizza.CheesePizza;
import com.atguigu.gulimall.product.factory.pizza.GreekPizza;
import com.atguigu.gulimall.product.factory.pizza.Pizza;
import lombok.extern.slf4j.Slf4j;

/**
 * @author chenlei
 * @date 2021-12-28 16:22
 * @description
 */
@Slf4j
public class SimpleFactory {

    /**
     * 根据orderType
     * @param orderType
     * @return
     */
    public Pizza createPizza(String orderType){
        log.info("简单工厂模式....");
        Pizza pizza = null;
        if("greek".equals(orderType)){
            pizza = new GreekPizza();
            pizza.setName("greekName");
        }else if("cheek".equals(orderType)){
            pizza = new CheesePizza();
            pizza.setName("cheekName");
        }
        return pizza;
    }
}
