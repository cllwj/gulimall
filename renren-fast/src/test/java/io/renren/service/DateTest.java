package io.renren.service;


import java.util.List;

/**
 * @author chenlei
 * @date 2021-09-16 10:54
 * @description
 */
public class DateTest {

    public static void main(String[] args) {
        String dateTimeFormatPattern = "yyyy-MM-dd HH:mm:ss";
        String dateFormatPattern = "yyyy-MM-dd";
        String startTime = "2021-09-15 00:01:00";
        String endTime = "2021-09-16 00:01:00";
        List<String> dateStrs = DateUtil.getAllDateStrsBetween(startTime,endTime,dateTimeFormatPattern,dateFormatPattern,false);
        System.out.println("----" + dateStrs);
    }
}
