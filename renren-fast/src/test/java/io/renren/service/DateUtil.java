package io.renren.service;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author chenlei
 * @date 2021-09-16 10:54
 * @description
 */
public class DateUtil {
    public DateUtil() {
    }

    public static Date parseDateTime(String dateTime, String format) {
        Date date = new Date();
        ZoneId zoneId = ZoneId.systemDefault();
        if (StringUtils.isNotEmpty(dateTime) && StringUtils.isNotEmpty(format)) {
            DateTimeFormatter dateTimeFormatter = (new DateTimeFormatterBuilder()).appendPattern(format).parseDefaulting(ChronoField.YEAR_OF_ERA, (long) LocalDate.now().getYear()).parseDefaulting(ChronoField.MONTH_OF_YEAR, (long)LocalDate.now().getMonthValue()).parseDefaulting(ChronoField.DAY_OF_MONTH, (long)LocalDate.now().getDayOfMonth()).parseDefaulting(ChronoField.HOUR_OF_DAY, 0L).parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0L).parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0L).parseDefaulting(ChronoField.MILLI_OF_SECOND, 0L).parseDefaulting(ChronoField.NANO_OF_SECOND, 0L).toFormatter();
            LocalDateTime localDateTime = LocalDateTime.parse(dateTime, dateTimeFormatter);
            date = Date.from(localDateTime.atZone(zoneId).toInstant());
        }

        return date;
    }

    public static List<String> getAllDateStrsBetween(String startDate, String endDate, String format) {
        return getAllDateStrsBetween(startDate, endDate, format, format, true);
    }

    public static List<String> getAllDateStrsBetween(String startDate, String endDate, String originalFormat,String dateFormat,boolean includeEnd) {
        // 返回的日期集合
        List<String> days = new ArrayList<>();
        Date start = parseDateTime(startDate, originalFormat);
        Date end = parseDateTime(endDate, originalFormat);
        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(start);
        tempStart.set(Calendar.HOUR_OF_DAY,0);
        tempStart.set(Calendar.MINUTE,0);
        tempStart.set(Calendar.SECOND,0);
        tempStart.set(Calendar.MILLISECOND,0);
        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(end);
        tempEnd.set(Calendar.HOUR_OF_DAY,23);
        tempEnd.set(Calendar.MINUTE,59);
        tempEnd.set(Calendar.SECOND,59);
        tempEnd.set(Calendar.MILLISECOND,999);
        while (!tempStart.after(tempEnd)) {
            days.add(formatDateTime(tempStart.getTime(), dateFormat));
            tempStart.add(Calendar.DAY_OF_YEAR, 1);
        }
        return days;
    }

    public static int getBetweenTimeType(Date endDate, Date startDate, TimeUnit timeType) {
        long num = 0L;
        if (endDate != null && startDate != null) {
            try {
                ZoneId zoneId = ZoneId.systemDefault();
                LocalDateTime endTime = endDate.toInstant().atZone(zoneId).toLocalDateTime();
                LocalDateTime startTime = startDate.toInstant().atZone(zoneId).toLocalDateTime();
                Duration duration = Duration.between(startTime, endTime);
                switch(timeType) {
                    case DAYS:
                        num = duration.toDays();
                        break;
                    case HOURS:
                        num = duration.toHours();
                        break;
                    case MINUTES:
                        num = duration.toMinutes();
                        break;
                    case SECONDS:
                        num = duration.getSeconds();
                }
            } catch (Exception var9) {
                var9.printStackTrace();
            }

            return (int)num;
        } else {
            return 0;
        }
    }

    public static int compareTime(String startDate, String endDate, String format) {
        if (!StringUtils.isEmpty(startDate) && !StringUtils.isEmpty(endDate)) {
            Date startTime = new Date();
            Date endTime = new Date();

            try {
                startTime = parseDateTime(startDate, format);
                endTime = parseDateTime(endDate, format);
            } catch (Exception var6) {
                var6.printStackTrace();
            }

            return startTime.compareTo(endTime);
        } else {
            return 0;
        }
    }

    public static Date addTimeTypeToDate(Date dateTime, int n, TimeUnit timeType) {
        if (dateTime == null) {
            return new Date();
        } else if (n == 0) {
            return dateTime;
        } else {
            ZoneId zoneId = ZoneId.systemDefault();
            LocalDateTime localDateTime = dateTime.toInstant().atZone(zoneId).toLocalDateTime();
            switch(timeType) {
                case DAYS:
                    localDateTime = localDateTime.plusDays((long)n);
                    break;
                case HOURS:
                    localDateTime = localDateTime.plusHours((long)n);
                    break;
                case MINUTES:
                    localDateTime = localDateTime.plusMinutes((long)n);
                    break;
                case SECONDS:
                    localDateTime = localDateTime.plusSeconds((long)n);
            }

            Date date = Date.from(localDateTime.atZone(zoneId).toInstant());
            return date;
        }
    }

    public static String formatDateTime(Date date, String format) {
        String formatTime = null;
        String patter = format;
        if (format == null || "".equals(format)) {
            patter = "yyyy-MM-dd HH:mm:ss";
        }

        SimpleDateFormat dateTimeFormatter = new SimpleDateFormat(patter);
        if (date != null) {
            formatTime = dateTimeFormatter.format(date);
        }

        return formatTime;
    }
}
