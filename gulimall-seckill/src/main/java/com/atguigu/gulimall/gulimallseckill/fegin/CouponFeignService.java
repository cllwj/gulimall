package com.atguigu.gulimall.gulimallseckill.fegin;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author chenlei
 * @date 2021-12-02 16:09
 * @description
 */
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    @GetMapping("/coupon/seckillsession/getLastes3DaySession")
    R getLastes3DaySession();
}
