package com.atguigu.gulimall.gulimallseckill.controller;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.gulimallseckill.service.SeckillService;
import com.atguigu.gulimall.gulimallseckill.to.SeckillSkuRedisTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-12-03 16:43
 * @description
 */
@RestController
@RequestMapping("/seckill/")
public class SeckillController {

    @Autowired
    SeckillService seckillService;

    @GetMapping("/currentSeckillSkuInfo")
    public R currentSeckillSkuInfo(){
       List<SeckillSkuRedisTo> data = seckillService.currentSeckillSkuInfo();
        return R.ok().setData(data);
    }
}
