package com.atguigu.gulimall.gulimallseckill.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author chenlei
 * @date 2021-12-03 9:32
 * @description
 */
@Data
public class SeckillSessionVo {

    private Long id;
    /**
     * 场次名称
     */
    private String name;
    /**
     * 每日开始时间
     */
    private Date startTime;
    /**
     * 每日结束时间
     */
    private Date endTime;
    /**
     * 启用状态
     */
    private Integer status;
    /**
     * 创建时间
     */
    private Date createTime;

    private List<SeckillSkuRelationVo> skuRelationEntities;
}
