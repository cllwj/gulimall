package com.atguigu.gulimall.gulimallseckill.service;

import com.atguigu.gulimall.gulimallseckill.to.SeckillSkuRedisTo;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-12-03 9:47
 * @description
 */
public interface SeckillService {

    void uploadSeckillSkuLatest3Days();

    List<SeckillSkuRedisTo> currentSeckillSkuInfo();
}
