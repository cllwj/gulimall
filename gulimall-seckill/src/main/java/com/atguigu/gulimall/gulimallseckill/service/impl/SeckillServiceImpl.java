package com.atguigu.gulimall.gulimallseckill.service.impl;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.gulimallseckill.fegin.CouponFeignService;
import com.atguigu.gulimall.gulimallseckill.fegin.ProductFeginService;
import com.atguigu.gulimall.gulimallseckill.service.SeckillService;
import com.atguigu.gulimall.gulimallseckill.to.SeckillSkuRedisTo;
import com.atguigu.gulimall.gulimallseckill.vo.SeckillSessionVo;
import com.atguigu.gulimall.gulimallseckill.vo.SkuInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.swing.text.BadLocationException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author chenlei
 * @date 2021-12-03 9:47
 * @description
 */
@Slf4j
@Service
public class SeckillServiceImpl implements SeckillService {

    @Autowired
    CouponFeignService couponFeignService;

    @Autowired
    ProductFeginService productFeginService;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    RedissonClient redissonClient;

    /**
     * 活动信息key前缀
     */
    private static final String SESSION_CACHE_PREFIX = "seckill:session:";

    private static final String SKUKILL_CACHE_PREFIX = "seckill:skus";

    private static final String SKUKILL_STOCK_SEMAPHORE = "seckill:stock:";


    /**
     * 上架最近三天的活动信息及对应的商品信息
     */
    @Override
    public void uploadSeckillSkuLatest3Days() {
        R r = couponFeignService.getLastes3DaySession();
        if(r.getCode() == 0){
            List<SeckillSessionVo> data = r.getData(new TypeReference<List<SeckillSessionVo>>() {
            });
            // 保存最近三天的活动信息到redis中
            saveSessionInfos(data);
            // 保存最近三天活动对应的商品信息到redis中
            saveSessionSkus(data);
        }
    }

    /**
     * sentinel降级后调用的方法
     * @return
     */
    public List<SeckillSkuRedisTo> blockHandlerSeckillSkuInfo(BlockException e){
        log.error("currentSeckillSkuInfo方法被限流啦.....");
        return null;
    }

    /**
     * 获取当前时间的秒杀商品信息
     * 1、获取当前时间（Long）
     * 2、获取redis中已上架的秒杀活动时间
     * 3、判断当前时间是否在秒杀时间范围内
     *      3.1 如果在这个时间范围内，则通过key获取value值
     *          再通过value到seckill:skus（HASH）中得到商品的信息
     * @return
     */
    @SentinelResource(value = "getCurrentSeckillSkuInfoResource",blockHandler = "blockHandlerSeckillSkuInfo")
    @Override
    public List<SeckillSkuRedisTo> currentSeckillSkuInfo() {
        Long date = System.currentTimeMillis();

        try (Entry entry = SphU.entry("seckillSkus")){
            Set<String> keys = redisTemplate.keys(SESSION_CACHE_PREFIX + "*");
            for (String key : keys) {
                // seckill:session:1638461700000_1638471599000
                String replace = key.replace(SESSION_CACHE_PREFIX, "");
                // 分割
                String[] split = replace.split("_");
                long start = Long.parseLong(split[0]);
                long end = Long.parseLong(split[1]);

                if(date >= start && date <= end){
                    List<String> range = redisTemplate.opsForList().range(key, -100, 100);
                    BoundHashOperations<String, String, String> ops = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
                    List<String> list = ops.multiGet(range);
                    if(list != null && !list.isEmpty()){
                        List<SeckillSkuRedisTo> collect = list.stream().map(item -> {
                            SeckillSkuRedisTo skuRedisTo = JSON.parseObject(item, SeckillSkuRedisTo.class);
                            return skuRedisTo;
                        }).collect(Collectors.toList());
                        return collect;
                    }
                    break;
                }
            }
        } catch (BlockException e) {
            log.error("该接口已限流处理！");
        }
        return null;
    }

    /**
     * 保存最近三天的活动信息到redis中
     * @param seckillSessionVos
     */
    private void saveSessionInfos(List<SeckillSessionVo> seckillSessionVos){
        seckillSessionVos.forEach(session->{
            Long startTime = session.getStartTime().getTime();
            Long endTime = session.getEndTime().getTime();
            String key = SESSION_CACHE_PREFIX + startTime + "_" + endTime;
            // 首先判断redis中是否存在这个key,如果存在则不保存，否则保存
            Boolean hasKey = redisTemplate.hasKey(key);
            if(!hasKey){
                List<String> collect = session.getSkuRelationEntities().stream().map(item->
                        item.getId() + "_" + item.getSkuId().toString()).collect(Collectors.toList());
                // 缓存活动信息
                redisTemplate.opsForList().leftPushAll(key,collect);
            }
        });
    }

    /**
     * 保存最近三天活动对应的商品信息到redis中
     * @param seckillSessionVos
     */
    private void saveSessionSkus(List<SeckillSessionVo> seckillSessionVos){
        seckillSessionVos.stream().forEach(session -> {
            BoundHashOperations<String, Object, Object> boundHashOps = redisTemplate.boundHashOps(SKUKILL_CACHE_PREFIX);
            session.getSkuRelationEntities().stream().forEach(seckillSkuVo -> {
                String randomCode = UUID.randomUUID().toString().replace("-", "");
               if(!boundHashOps.hasKey(seckillSkuVo.getPromotionSessionId().toString() + "_" + seckillSkuVo.getSkuId().toString())){
                    // 缓存商品 key:商品ID  value:商品信息和sku详细信息（JSON）
                   SeckillSkuRedisTo skuRedisTo = new SeckillSkuRedisTo();
                   // 1、sku基本信息
                   R skuInfo = productFeginService.getSkuInfo(seckillSkuVo.getSkuId());
                   if(skuInfo.getCode() == 0){
                       SkuInfoVo info = skuInfo.getData("skuInfo", new TypeReference<SkuInfoVo>() {
                       });
                       skuRedisTo.setSkuInfo(info);
                   }
                   // 2、sku秒杀信息
                   BeanUtils.copyProperties(seckillSkuVo,skuRedisTo);

                   // 3、设置商品活动的秒杀时间
                   skuRedisTo.setStartTime(session.getStartTime().getTime());
                   skuRedisTo.setEndTime(session.getEndTime().getTime());

                   // 4、商品的随机码，为每一个商品设置随机码
                   skuRedisTo.setRandomCode(randomCode);
                   String jsonString = JSON.toJSONString(skuRedisTo);
                   log.info("商品信息上传....");
                   boundHashOps.put(seckillSkuVo.getPromotionSessionId().toString() + "_" + seckillSkuVo.getSkuId().toString(),jsonString);

                   // 5、商品分布式信号量
                   // 缓存里面只有100件商品，假如有100万个请求来秒杀，结果还是这100件商品到数据库里面扣减库存，所有就引入到分布式信号量
                   // 每一个商品都有一个信号量，请求过来想要秒杀该商品，则首先获取该商品的信号量，如果信号量能减了，则放行，再处理后面的数据库操作
                   // 那么整个请求就能得到很快的释放
                   log.info("商品价格信号量上传....");
                   RSemaphore semaphore = redissonClient.getSemaphore(SKUKILL_STOCK_SEMAPHORE + randomCode);
                   // 商品可以秒杀的数量作为信号量
                   semaphore.trySetPermits(seckillSkuVo.getSeckillCount().intValue());
               }
            });
        });

    }
}
