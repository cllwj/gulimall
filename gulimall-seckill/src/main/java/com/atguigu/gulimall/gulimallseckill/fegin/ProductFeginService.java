package com.atguigu.gulimall.gulimallseckill.fegin;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author chenlei
 * @date 2021-12-03 11:32
 * @description
 */
@FeignClient("gulimall-product")
public interface ProductFeginService {

    @RequestMapping("/product/skuinfo/info/{skuId}")
    R getSkuInfo(@PathVariable("skuId") Long skuId);
}
