package com.atguigu.gulimall.gulimallseckill.scheduled;

import com.atguigu.gulimall.gulimallseckill.service.SeckillService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author chenlei
 * @date 2021-12-02 15:27
 * @description 秒杀商品定时上架
 *      每天晚上3点：上架最近三天的商品
 */

@Slf4j
@Service
public class SeckillScheduled {

    @Autowired
    SeckillService seckillService;

    @Autowired
    RedissonClient redissonClient;

    public static final String UPLOAD_LOCK = "seckill:upload:lock";

    @Scheduled(cron = "* * 1 * * ?")
    public void uploadSeckillSkuLatest3Day(){
        // 添加分布式锁，防止在分布式的情况下，重复上架,无论成功如否，都需要进行解锁操作
        RLock lock = redissonClient.getLock(UPLOAD_LOCK);
        lock.lock(10, TimeUnit.SECONDS);
        try {
            seckillService.uploadSeckillSkuLatest3Days();
        } finally {
            lock.unlock();
        }
    }

}
