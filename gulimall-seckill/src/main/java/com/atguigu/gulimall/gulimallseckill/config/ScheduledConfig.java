package com.atguigu.gulimall.gulimallseckill.config;

import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

/**
 * @author chenlei
 * @date 2021-12-02 15:27
 * @description
 */
@Component
@EnableAsync
@EnableScheduling
public class ScheduledConfig {
}
