package com.atguigu.gulimall.gulimallseckill.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author chenlei
 * @date 2021-09-18 15:04
 * @description
 */
@Configuration
public class MyRedissonConfig {

    /**
     * 所有对redisson的使用都是通过redissonClient对象
     * @return
     */
    @Bean(destroyMethod = "shutdown")
    public RedissonClient redissonClient(){
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.18.38:6379");
        RedissonClient redissonClient = Redisson.create(config);
        return redissonClient;
    }
}
