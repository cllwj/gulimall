package com.atguigu.gulimall.gulimallseckill.scheduled;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务的使用：
 * 1、@EnableScheduling 开启定时任务
 * 2、@Scheduled 开启一个定时任务
 *
 * 异步任务：
 *      1、@EnableAsync 开启异步任务功能
 *      2、@Async 给希望异步执行的方法上标注
 *      3、自动配置类
 */
@Slf4j
@Component
public class HelloScheduled {

    @Async
    // @Scheduled(cron = "0/1 * * * * ?")
    public void hello() throws InterruptedException {
        log.info("hello...");
        Thread.sleep(3000);
    }
}
