package com.atguigu.gulimall.gulimallseckill;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

//@SpringBootTest()
public class GulimallSeckillApplicationTests {

    @Test
    public void testTime(){
        /**
         * 获取当前日期和当前日期加3天日期
         */
        LocalDate now = LocalDate.now();
        LocalDate localDate1 = now.plusDays(1);
        LocalDate localDate2 = now.plusDays(2);

        System.out.println(now);
        System.out.println(localDate1);
        System.out.println(localDate2);
        System.out.println("------------------------");
        LocalDateTime now1 = LocalDateTime.now();
        System.out.println(now1);

        /**
         * 获取最大时间和最小时间
         */
        LocalTime min = LocalTime.MIN;
        LocalTime max = LocalTime.MAX;
        System.out.println(min); // 00:00
        System.out.println(max); // 23:59:59.999999999

        /**
         * 拼接日期和时间,时间格式化
         */
        LocalDateTime start = LocalDateTime.of(now, min);
        LocalDateTime end = LocalDateTime.of(localDate2, max);
        String startFormat = start.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String endFormat = end.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        System.out.println(startFormat); // 2021-12-02 00:00:00
        System.out.println(endFormat); // 2021-12-04 23:59:59
    }

}
