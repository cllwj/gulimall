package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author cl
 * @email sunlightcs@gmail.com
 * @date 2021-08-15 08:47:28
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
