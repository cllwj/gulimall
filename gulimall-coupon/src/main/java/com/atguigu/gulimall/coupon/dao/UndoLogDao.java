package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author cl
 * @email sunlightcs@gmail.com
 * @date 2021-08-15 08:47:28
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
