package com.atguigu.gulimall.gulimallcoupon;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// @SpringBootTest
class GulimallCouponApplicationTests {

    @Test
    void contextLoads() {
    }

    public void test(){
        String contect = "尊敬的${name}您好！,请在${data}前来公司参加面试，请注意查收！";

    }

    public static void main(String[] args) {
         renderString();
        /*String content = "hello ${name}, 1 2 3 4 5 ${six} 7, again ${name}. ";
        String contentInfo = getContentInfo(content);
        List<String> list = Arrays.asList(contentInfo);
        System.out.println("----------> " + list);*/
    }

    public static void renderString() {
        String content = "hello ${name}, 1 2 3 4 5 ${six} 7, again ${name}. ";
        Map<String, String> map = new HashMap<>();
        map.put("name", "java");
        map.put("name", "java");
        map.put("six", "6");
        map.put("six", "6");
        content = renderString(content, map);
        System.out.println(content);
    }

    public static String renderString(String content, Map<String, String> map){
        Set<Map.Entry<String, String>> sets = map.entrySet();
        for(Map.Entry<String, String> entry : sets) {
            String regex = "\\$\\{" + entry.getKey() + "\\}";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(content);
            content = matcher.replaceAll(entry.getValue());
        }
        return content;
    }

    public static String getContentInfo(String content) {
        Pattern regex = Pattern.compile("\\$\\{([^}]*)\\}");
        Matcher matcher = regex.matcher(content);
        StringBuilder sql = new StringBuilder();
        while(matcher.find()) {
            sql.append(matcher.group(1)+",");
        }
        if (sql.length() > 0) {
            sql.deleteCharAt(sql.length() - 1);
        }
        return sql.toString();
    }

}
