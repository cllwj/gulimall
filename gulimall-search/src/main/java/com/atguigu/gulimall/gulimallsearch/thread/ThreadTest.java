package com.atguigu.gulimall.gulimallsearch.thread;

import jdk.nashorn.internal.codegen.CompilerConstants;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author chenlei
 * @date 2021-10-14 15:17
 * @description
 */
@Slf4j
public class ThreadTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        log.info("mian start............");
        /**
         * 1）、继承 Thread
         * 2）、实现 Runnable 接口
         * 3）、实现 Callable 接口 + FutureTask （可以拿到返回结果，可以处理异常）
         * 4）、线程池
         */

        // 1.继承 Thread
        /*log.info("mian start............");
        Thread01 thread01 = new Thread01();
        thread01.start(); // 启动线程
        log.info("mian end............");*/

        // 2、实现runnable接口
        /*Runnable01 runnable01 = new Runnable01();
        new Thread(runnable01).start();*/

        // 3、实现 Callable 接口 + FutureTask
        /*FutureTask<Integer> futureTask = new FutureTask<>(new Callable01());
        new Thread(futureTask).start();
        //  阻塞等待整个线程执行完，获取返回结果
        Integer integer = futureTask.get();

        log.info("mian end............" + integer);*/

        // 线程池   给线程池直接提交任务
        /**
         * 以上三种方式启动线程的方式缺点是比较吃cpu资源
         * 【将所有的多线程异步任务都交给线程池执行】
         */


    }

    public static class Thread01 extends Thread{
        @Override
        public void run() {
            log.info("当前线程：" + Thread.currentThread().getId());
            int i = 10/2;
            log.info("运行结果：{}",i);
        }
    }

    public static class Runnable01 implements Runnable{

        @Override
        public void run() {
            log.info("当前线程：" + Thread.currentThread().getId());
            int i = 10/2;
            log.info("运行结果：{}",i);
        }
    }

    public static class Callable01 implements Callable<Integer>{

        @Override
        public Integer call() throws Exception {
            log.info("当前线程：" + Thread.currentThread().getId());
            int i = 10/2;
            log.info("运行结果：{}",i);
            return i;
        }
    }
}
