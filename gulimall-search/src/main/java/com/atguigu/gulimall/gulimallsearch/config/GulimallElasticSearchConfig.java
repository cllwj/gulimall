package com.atguigu.gulimall.gulimallsearch.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.number.PercentStyleFormatter;

/**
 * @author chenlei
 * @date 2021-09-07 21:12
 * @description
 */
@Configuration
public class GulimallElasticSearchConfig {

    public static final RequestOptions COMMON_OPTIONS;

    static {
        RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
        COMMON_OPTIONS = builder.build();
    }

    @Bean
    public RestHighLevelClient esClient(){
        RestClientBuilder builder = RestClient.builder(new HttpHost("192.168.146.129", 9200, "http"));
        return new RestHighLevelClient(builder);
    }
}
