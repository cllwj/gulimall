//package com.atguigu.gulimall.gulimallsearch.email;
//
///**
// * @author chenlei
// * @date 2021-10-19 11:55
// * @description
// */
//
//import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.MimeBodyPart;
//import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.MimeMultipart;
//import com.sun.xml.internal.messaging.saaj.packaging.mime.internet.MimeUtility;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.stereotype.Component;
//import org.synchronoss.cloud.nio.multipart.Multipart;
//
//import javax.activation.DataHandler;
//import javax.activation.FileDataSource;
//import javax.websocket.Session;
//import javax.mail.*;
//import javax.mail.internet.*;
//import java.util.*;
//
///**
// * @author chenlei
// * @date 2021-07-22 16:22
// * @description  发送Email消息
// */
//@Slf4j
//@Component
//public class SendMail {
//
//    /**
//     * MIME邮件对象
//     */
//    private MimeMessage mimeMsg;
//
//    /**
//     * 邮件会话对象
//     */
//    private Session session;
//
//    /**
//     * 系统属性
//     */
//    private Properties props;
//
//    /**
//     * smtp是否需要认证
//     */
//    private boolean needAuth = false;
//
//    /**
//     * smtp认证用户名和密码
//     */
//    private String username;
//    private String password;
//
//    /**
//     * Multipart对象,邮件内容,标题,附件等内容均添加到其中后再生成MimeMessage对象
//     */
//    private Multipart mp;
//
//    public SendMail(){}
//
//    /**
//     * @param smtp 邮件发送服务器
//     */
//    public SendMail(String smtp) {
//        setSmtpHost(smtp);
//        createMimeMessage();
//    }
//
//    /**
//     * 设置邮件发送服务器
//     *
//     * @param hostName String
//     */
//    public void setSmtpHost(String hostName) {
//        if (props == null) {
//            // 获得系统属性对象
//            props = System.getProperties();
//            // 设置SMTP主机
//            props.put("mail.smtp.host", hostName);
//        }
//    }
//
//    /**
//     * 创建MIME邮件对象
//     *
//     * @return
//     */
//    public boolean createMimeMessage() {
//        try {
//            // 获得邮件会话对象
//            session = Session.getDefaultInstance(props, null);
//        } catch (Exception e) {
//            return false;
//        }
//        try {
//            // 创建MIME邮件对象
//            mimeMsg = new MimeMessage(session);
//            mp = new MimeMultipart("related");
//
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    /**
//     * 设置SMTP是否需要验证
//     *
//     * @param need
//     */
//    public void setNeedAuth(boolean need) {
//        if (props == null) {
//            props = System.getProperties();
//        }
//        if (need) {
//            props.put("mail.smtp.auth", "true");
//        } else {
//            props.put("mail.smtp.auth", "false");
//        }
//    }
//
//    /**
//     * 设置用户名和密码
//     *
//     * @param name
//     * @param pass
//     */
//    public void setNamePass(String name, String pass) {
//        username = name;
//        password = pass;
//    }
//
//    /**
//     * 设置邮件主题
//     *
//     * @param mailSubject
//     * @return
//     */
//    public boolean setSubject(String mailSubject) {
//        try {
//            mimeMsg.setSubject(mailSubject);
//            mimeMsg.setSentDate(new Date());
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    /**
//     * 设置邮件正文
//     *
//     * @param mailBody String
//     */
//    public boolean setBody(String mailBody) {
//        try {
//            BodyPart bp = new MimeBodyPart();
//            bp.setContent(mailBody, "text/html;charset=GBK");
//            // bp.setDataHandler(new DataHandler(mailBody, "text/html;charset=GBK"));
//            mp.addBodyPart(bp);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    /**
//     * 添加附件
//     *
//     * @param filename String
//     */
//    public boolean addFileAffix(String filename) {
//
//        log.info("增加邮件附件：{}",filename);
//        try {
//            BodyPart bp = new MimeBodyPart();
//            FileDataSource fileds = new FileDataSource(filename);
//            bp.setDataHandler(new DataHandler(fileds));
//            bp.setFileName(fileds.getName());
//            // 解决附件名称乱码
//            bp.setFileName(MimeUtility.encodeText(fileds.getName(),"utf-8",null));
//            mp.addBodyPart(bp);
//            return true;
//        } catch (Exception e) {
//            log.info("增加邮件附件失败！");
//            return false;
//        }
//    }
//
//    /**
//     * 设置发信人
//     * @param from String
//     */
//    public boolean setFrom(String from) {
//        try {
//            // mimeMsg.setFrom(new InternetAddress(from)); // 设置发信人
//            // 设置发信人
//            mimeMsg.setFrom(from);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    /**
//     * 设置收信人（可群发）
//     * String[] mailArray
//     * @param to String
//     */
//    public boolean setTo(String to) {
//        if (to == null) {
//            return false;
//        }
//
//        try {
//            mimeMsg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    /**
//     * 可群发
//     * @param userArray
//     * @return
//     * @throws AddressException
//     */
//    public boolean setTo(List<User> userArray) throws AddressException {
//        if(userArray == null || userArray.isEmpty()){
//            log.error("收件人邮箱地址不能为空！");
//            return false;
//        }
//
//        int len = userArray.size();
//        InternetAddress[] address = new InternetAddress[len];
//        for (int j = 0; j < len; j++){
//            String addresseeEmail = userArray.get(j).getEmail();
//            if(StringUtils.isNotBlank(addresseeEmail)){
//                address[j] = new InternetAddress(addresseeEmail);
//            }
//        }
//
//        try {
//            mimeMsg.setRecipients(Message.RecipientType.TO, address);
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    /**
//     * 设置抄送人
//     * @param copyto String
//     */
//    public boolean setCopyTo(String copyto) {
//        if (StringUtils.isBlank(copyto)) {
//            return false;
//        }
//        try {
//            mimeMsg.setRecipients(Message.RecipientType.CC, (Address[]) InternetAddress.parse(copyto));
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    /**
//     * 发送邮件
//     */
//    public boolean sendOut() {
//        long start = System.currentTimeMillis();
//        try {
//            mimeMsg.setContent(mp);
//            mimeMsg.saveChanges();
//            Session mailSession = Session.getInstance(props, null);
//            Transport transport = mailSession.getTransport("smtp");
//            transport.connect((String) props.get("mail.smtp.host"), username, password);
//            transport.sendMessage(mimeMsg, mimeMsg.getRecipients(Message.RecipientType.TO));
//            // 如果有抄送人，则带抄送发送邮件
//            if (mimeMsg.getRecipients(Message.RecipientType.CC) != null) {
//                transport.sendMessage(mimeMsg, mimeMsg.getRecipients(Message.RecipientType.CC));
//            }
//            // transport.send(mimeMsg);
//            transport.close();
//            long end = System.currentTimeMillis();
//            log.info("发送邮件调用sendOut()方法用时：{}",(end-start)/1000);
//            return true;
//        } catch (Exception e) {
//            log.error("发送Email失败，失败原因：{}",e.getMessage());
//            return false;
//        }
//    }
//
//    /**
//     * 调用sendOut方法完成邮件发送,带附件和抄送
//     *
//     * @param smtp
//     * @param from
//     * @param to
//     * @param copyto
//     * @param subject
//     * @param content
//     * @param username
//     * @param password
//     * @param filename
//     * @return
//     */
//    public boolean send(String smtp, String from, List<User> to, String copyto, String subject, String content,
//                        String username, String password, String filename) throws AddressException {
//        SendMail theMail = new SendMail(smtp);
//        // 需要验证
//        theMail.setNeedAuth(true);
//
//        if (!theMail.setSubject(subject)) {
//            log.error("设置邮件主题失败！");
//            return false;
//        }
//
//        if (!theMail.setBody(content)) {
//            log.error("设置邮件正文失败！");
//            return false;
//        }
//
//        // 判断是否有附件
//        if(StringUtils.isNotBlank(filename)) {
//            if (!theMail.addFileAffix(filename)) {
//                log.error("添加附件失败！");
//                return false;
//            }
//        }
//
//        if (!theMail.setTo(to)) {
//            log.error("收件人Email有误！");
//            return false;
//        }
//
//        // 判断是否抄送
//        if(StringUtils.isNotBlank(copyto)){
//            if (!theMail.setCopyTo(copyto)) {
//                log.error("设置抄送人有误！");
//                return false;
//            }
//        }
//
//        if (!theMail.setFrom(from)) {
//            log.error("设置发信人有误！");
//            return false;
//        }
//
//        theMail.setNamePass(username, password);
//
//        if (!theMail.sendOut()) {
//            log.error("发送邮件失败！");
//            return false;
//        }
//        return true;
//    }
//
//    /**
//     * 调用sendOut方法完成邮件发送,带附件
//     *
//     * @param smtp
//     * @param from
//     * @param to
//     * @param subject
//     * @param content
//     * @param username
//     * @param password
//     * @param filename 附件路径
//     * @return
//     */
//    public boolean send(String smtp, String from, String to, String subject, String content, String username,
//                        String password, String filename) {
//        SendMail theMail = new SendMail(smtp);
//        // 需要验证
//        theMail.setNeedAuth(true);
//
//		/*if (!theMail.setSubject(subject)) {
//			return false;
//		}*/
//
//        if (!theMail.setBody(content)) {
//            return false;
//        }
//
//        // 判断是否有附件
//        if(StringUtils.isNotBlank(filename)) {
//            if (!theMail.addFileAffix(filename)) {
//                log.error("添加附件失败！");
//                return false;
//            }
//        }
//
//        if (!theMail.setTo(to)) {
//            return false;
//        }
//        if (!theMail.setFrom(from)) {
//            return false;
//        }
//        theMail.setNamePass(username, password);
//
//        if (!theMail.sendOut()) {
//            return false;
//        }
//        return true;
//    }
//
//
//    /**
//     * String smtp, String from, String to, String subject, String content, String username,
//     * 							   String password, String filename
//     * @param args
//     * @throws AddressException
//     */
//    public static void main(String[] args){
//        // 接口调用
//        List<String> mailArray = new ArrayList<>();
//        mailArray.add("3208697230@qq.com");
//        // mailArray.add("3260989926@qq.com");
//        SendMail sendMail = new SendMail();
//        boolean isSuccess = sendMail.send("smtp.qq.com",
//                "3208697230@qq.com",
//                "3208697230@qq.com",
//                "邮件测试sss",
//                "邮件测试",
//                "3208697230@qq.com",
//                "ezzduglmwezldgga",
//                "E:\\三好学生1.txt");
//        if(isSuccess){
//            log.info("发送Email成功！");
//        }else{
//            log.info("发送Email失败！");
//        }
//    }
//}
//
