package com.atguigu.gulimall.gulimallsearch.thread;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

/**
 * @author chenlei
 * @date 2021-10-14 17:15
 * @description
 */
@Slf4j
public class CompletableTest {

   public static ExecutorService executor = Executors.newFixedThreadPool(10);

    private static final Whitelist CUSTOM_WHITELIST = Whitelist.relaxed()
            .addAttributes("video", "width", "height", "controls", "alt", "src")
            .addAttributes(":all", "style", "class");

    /**
     * jsoup不格式化代码
     */
    private static final Document.OutputSettings OUTPUT_SETTINGS = new Document.OutputSettings().prettyPrint(false);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        String html = "<p style=\"text-align: left;\"><span style=\"text-decoration: underline;\"><em><strong>sdfsdfs</strong></em></span><span style=\"text-decoration: none;\"></span><br/></p>";
        String doc = Jsoup.clean(html,"", CUSTOM_WHITELIST,OUTPUT_SETTINGS);
        log.info("处理后的参数为：{}",doc);
    }
}
