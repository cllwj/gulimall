package com.atguigu.gulimall.gulimallsearch.thread;

import java.util.concurrent.*;

/**
 * @author chenlei
 * @date 2021-10-14 15:46
 * @description
 */
public class ThreadTest02 {
    public static void main(String[] args) {
        /**
         * 线程池
         * 1、创建
         *    1）new ThreadPoolExecutor()
         * 2、七大参数
         *     int corePoolSize：核心线程数【一直存在】，线程池，创建好以后就准备就绪的线程数量，就等待来接收异步任务去执行
         *     int maximumPoolSize：最大线程数，作用：控制资源
         *     long keepAliveTime：存活时间，如果当前的线程数量大于核心数量。释放空闲的线程（超出核心线程的线程部分，核心线程是不释放的），只要线程空闲大于指定的keepAliveTime
         *     TimeUnit unit：时间单位
         *     BlockingQueue<Runnable> workQueue：阻塞队列，如果任务有很多，就会将目前多的任务放在队列里面
         *                                        只要有线程空闲，就会去队列里面取出新的任务继续执行
         *     ThreadFactory threadFactory：线程的创建工厂
         *     RejectedExecutionHandler handler：如果队列满啦，按照我们指定的拒绝策略执行任务
         *
         *  3、工作顺序：
         *    1）线程池创建，准备好核心线程，准备接收任务
         *      1.1）任务存放顺序，先是判断核心数，如果小于核心数就存入核心线程池中
         *      1.2）如果大于核心数，就会将进来的任务放入阻塞队列中，空闲的核心线程就会去阻塞队列中获取任务执行
         *      1.3）当阻塞队列已满，就直接开新的线程执行，最大只能开到max指定的数量
         *      1.4）max都执行完成，有很多空闲的线程，在指定的存活时间keepAliveTime以后，就会释放（max-core）这一部分的线程，核心线程不会释放
         *      new LinkedBlockingDeque<>(100000): 默认是Integer的最大值，内存不足，需要在创建的时候指定大小
         *
         *  面试：
         * 一个线程池 core 7； max 20 ，queue：50，100 并发进来怎么分配的；
         * 先有 7 个能直接得到执行，接下来 50 个进入队列排队，在多开 13 个继续执行。现在 70 个
         * 被安排上了。剩下 30 个默认拒绝策略。
         *
         *
         */
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                5,
                200,
                10,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(100000),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());

//        Executors.newCachedThreadPool(); core是0，所有都可回收
//        Executors.newFixedThreadPool(10); 固定大小，core=max,都不可回收
//        Executors.newScheduledThreadPool(10); 定时任务的线程池
//        Executors.newSingleThreadExecutor(); 单线程的线程池，后台从队列里面获取任务，一个一个执行


    }
}
