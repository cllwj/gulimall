package com.atguigu.gulimall.gulimallsearch.service;

import com.atguigu.gulimall.gulimallsearch.vo.SearchParam;
import com.atguigu.gulimall.gulimallsearch.vo.SearchResult;

/**
 * @author chenlei
 * @date 2021-10-13 17:10
 * @description
 */
public interface MallSearchService {

    /**
     *
     * @param param 检索的所有参数
     * @return 返回检索的结果
     */
    SearchResult search(SearchParam param);
}
