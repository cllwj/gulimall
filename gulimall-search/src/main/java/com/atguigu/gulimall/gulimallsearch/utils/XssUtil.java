package com.atguigu.gulimall.gulimallsearch.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

import java.util.Map;
import java.util.Objects;

/**
 * xss拦截工具类
 *
 * @author lieber
 */
@Slf4j
public enum XssUtil {


    /**
     * 实例
     */
    INSTANCE;

    private static final String RICH_TEXT = "</";

    /**
     * 自定义白名单
     * .addAttributes(":all", "style", "class");
     */
    private static final Whitelist CUSTOM_WHITELIST = Whitelist.relaxed()
            .addAttributes("video", "width", "height", "controls", "alt", "src")
            .addAttributes(":all", "style", "class");

    /**
     * jsoup不格式化代码
     */
    private static final Document.OutputSettings OUTPUT_SETTINGS = new Document.OutputSettings().prettyPrint(false);

    /**
     * 清除json对象中的xss攻击字符
     *
     * @param val json对象字符串
     * @return 清除后的json对象字符串
     */
    private Object cleanObj(Object val) {
        JSONObject jsonObject;
        if(val instanceof String){
            jsonObject = JSON.parseObject((String)val);
        }else if(val instanceof JSONObject){
            jsonObject = (JSONObject) val;
        }else{
            return val;
        }
        for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
            if (entry.getValue() instanceof String) {
                Object obj = entry.getValue();
                obj = this.cleanXss(obj);
                entry.setValue(obj);
            }
        }
        return jsonObject;
    }

    /**
     * 清除json数组中的xss攻击字符
     *
     * @param val json数组字符串
     * @return 清除后的json数组字符串
     */
    private Object cleanArr(Object val) {
        JSONArray original;
        if(val instanceof String){
            original = JSON.parseArray((String)val);
        }else if(val instanceof JSONArray) {
            original =(JSONArray) val;
        }else{
            return val;
        }
        JSONArray result = new JSONArray(original.size());
        for (Object str : original) {
            str = this.cleanXss(str);
            result.add(str);
        }
        return result;
    }

    /**
     * 清除xss攻击字符串，此处优化空间较大
     *
     * @param str 字符串
     * @return 清除后无害的字符串
     */
    public Object cleanXss(Object obj) {
        if (JsonUtil.INSTANCE.isJsonObj(obj)) {
            return this.cleanObj(obj);
        } else if (JsonUtil.INSTANCE.isJsonArr(obj)) {
            return this.cleanArr(obj);
        } else {
            String str = (String) obj;
            boolean richText = this.richText(str);
            if (!richText) {
                str = str.trim();
                str = str.replaceAll(" +", " ");
            }
            String afterClean = Jsoup.clean(str, "", CUSTOM_WHITELIST, OUTPUT_SETTINGS);
            if (paramError(richText, afterClean, str)) {
                log.info("参数包含特殊字符");
                return null;
            }
            str = richText ? afterClean : this.backSpecialStr(afterClean);
            return str;
        }
    }


    /**
     * 判断是否是富文本
     *
     * @param str 待判断字符串
     * @return true/false
     */
    private boolean richText(String str) {
        return str.contains(RICH_TEXT);
    }

    /**
     * 判断是否参数错误
     *
     * @param richText   是否富文本
     * @param afterClean 清理后字符
     * @param str        原字符串
     * @return true/false
     */
    private boolean paramError(boolean richText, String afterClean, String str) {
        // 如果包含富文本字符，那么不是参数错误
        if (richText) {
            return false;
        }
        // 如果清理后的字符和清理前的字符匹配，那么不是参数错误
        if (Objects.equals(str, afterClean)) {
            return false;
        }
        // 如果仅仅包含可以通过的特殊字符，那么不是参数错误
        if (Objects.equals(str, this.backSpecialStr(afterClean))) {
            return false;
        }
        // 如果还有......
        return true;
    }

    /**
     * 转义回特殊字符
     *
     * @param str 已经通过转义字符
     * @return 转义后特殊字符
     */
    private String backSpecialStr(String str) {
        return str.replace("&amp;", "&");
    }
}
