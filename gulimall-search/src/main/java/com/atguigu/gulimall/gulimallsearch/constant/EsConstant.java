package com.atguigu.gulimall.gulimallsearch.constant;

/**
 * @author chenlei
 * @date 2021-09-12 14:52
 * @description
 */
public class EsConstant {

    /**
     * sku在es中的索引
     */
    public static final String PRODUCT_INDEX = "gulimall_product";

    public static final Integer PRODUCT_PAGESIZE = 16;

}
