package com.atguigu.gulimall.gulimallsearch;

import lombok.extern.slf4j.Slf4j;

/**
 * @author chenlei
 * @date 2021-10-21 11:12
 * @description
 * 转义前  <p style=\"text-align: center;\">sdf</p>
 * 转义后  <p style="\&quot;text-align:">sdf</p>
 */
@Slf4j
public class Test {


    public static void main(String[] args) {
        /*String str = "<p style=\"text-align: center;\">sdf</p>";
        Object obj = XssUtil.INSTANCE.cleanXss(str);
        log.info("处理后的参数为：{}",obj);
        log.info("处理后的参数长度为：{}",str.length());*/
    }
}
