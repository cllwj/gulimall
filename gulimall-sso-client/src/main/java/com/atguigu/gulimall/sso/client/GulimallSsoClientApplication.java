package com.atguigu.gulimall.sso.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GulimallSsoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallSsoClientApplication.class, args);
    }

}
