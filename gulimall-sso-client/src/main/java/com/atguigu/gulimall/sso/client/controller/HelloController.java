package com.atguigu.gulimall.sso.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chenlei
 * @date 2021-11-03 21:50
 * @description
 */
@Controller
public class HelloController {


    @Value("${sso.server.url}")
    private String ssoServer;

    /**
     * 无需登录即可访问
     * @return
     */
    @ResponseBody
    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    /**
     *
     * @param model
     * @param session
     * @param token 只要去ssoserver登录成功了才会有token信息，所以token不一定有值
     * @return
     */
    @GetMapping("/emps")
    public String emps(Model model, HttpSession session,
                       @RequestParam(value = "token",required = false) String token){
        if(!StringUtils.isEmpty(token)){
            // 获取用户登录的用户名
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> entity = restTemplate.getForEntity("http://ssoserver.com:8080/userInfo?token="+token, String.class);
            String body = entity.getBody();
            // 将token信息放到session中
            session.setAttribute("loginUser",body);
        }

        String loginUser = (String) session.getAttribute("loginUser");
        if(loginUser == null){
            // 没有登录，跳转到登录服务认证中心进行登录
            return "redirect:" + ssoServer + "?redirect_url=http://client1.com:8081/emps";
        }else{
            List<String> emps = new ArrayList<>();
            emps.add("张三");
            emps.add("李四");
            model.addAttribute("emps",emps);
            return "list";
        }
    }

}
