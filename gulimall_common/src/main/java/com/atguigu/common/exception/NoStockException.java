package com.atguigu.common.exception;

/**
 * @author chenlei
 * @date 2021-11-19 11:27
 * @description 自定义商品无库存异常类
 */
public class NoStockException extends RuntimeException{

    private Long skuId;

    public NoStockException(Long skuId){
        super("商品id:" + skuId + ";没有足够的库存啦！");
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }
}
