package com.atguigu.common.constant;

/**
 * @author chenlei
 * @date 2021-11-04 16:46
 * @description
 */
public class CartConstant {

    public static final String TEMP_USER_COOKIE_NAME = "user-key";
    public static final int TEMP_USER_COOKIE_TIMEOUT = 60*60*24*30;

}
