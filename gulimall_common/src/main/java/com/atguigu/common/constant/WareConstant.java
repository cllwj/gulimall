package com.atguigu.common.constant;

/**
 * @author chenlei
 * @date 2021-09-02 17:34
 * @description
 */
public class WareConstant {

    /**
     * 新建
     * 已分配
     * 已领取
     * 已完成
     * 有异常
     */
    public enum PurchaseEnum{
        CREATED(0,"新建"),ASSIGNED(1,"已分配"),
        RECEIVE(2,"已领取"),FINISH(3,"已完成"),
        HASERROR(4,"有异常");
        private int code;
        private String msg;

        PurchaseEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

    /**
     * 新建
     * 已分配
     * 正在采购
     * 已完成
     * 采购失败
     */
    public enum PurchaseDetailEnum{
        CREATED(0,"新建"),ASSIGNED(1,"已分配"),
        BUYING(2,"正在采购"),FINISH(3,"已完成"),
        HASERROR(4,"采购失败");
        private int code;
        private String msg;

        PurchaseDetailEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
}
