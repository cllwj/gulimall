package com.atguigu.common.to.mq;

import lombok.Data;


/**
 * @author chenlei
 * @date 2021-11-24 14:48
 * @description
 */
@Data
public class StockLockedTo {

    /**
     * 库存工作单id
     */
    private Long id;

    /**
     * 工作单详情
     */
    private OrderTaskDetailTo detailTo;

}
