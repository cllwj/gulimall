package com.atguigu.common.to;

import lombok.Data;

/**
 * @author chenlei
 * @date 2021-09-12 11:16
 * @description
 */
@Data
public class SkuHasStockVo {

    private Long skuId;
    private Boolean hasStock;
}
