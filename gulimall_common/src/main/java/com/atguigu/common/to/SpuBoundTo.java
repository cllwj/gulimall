package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author chenlei
 * @date 2021-09-02 10:00
 * @description
 */
@Data
public class SpuBoundTo {

    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
