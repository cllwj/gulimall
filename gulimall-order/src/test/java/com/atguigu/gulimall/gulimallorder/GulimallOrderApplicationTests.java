package com.atguigu.gulimall.gulimallorder;

import com.atguigu.gulimall.order.GulimallOrderApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GulimallOrderApplication.class)
public class GulimallOrderApplicationTests {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Test
    public void createExchange(){
        /**
         * 创建直接交换机
         * String name, 名称
         * boolean durable, 是否持久化
         * boolean autoDelete, 是否启动后删除
         * Map<String, Object> arguments 参数
         */
        DirectExchange directExchange = new DirectExchange("java.hello-exchange",true,false);
        amqpAdmin.declareExchange(directExchange);
        System.out.println("交换机【{}】创建成功");
    }
}
