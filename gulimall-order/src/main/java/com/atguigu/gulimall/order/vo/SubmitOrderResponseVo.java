package com.atguigu.gulimall.order.vo;

import com.atguigu.gulimall.order.entity.OrderEntity;
import lombok.Data;

/**
 * @author chenlei
 * @date 2021-11-16 18:00
 * @description
 */
@Data
public class SubmitOrderResponseVo {

    private OrderEntity order;

    /**
     * 0成功 错误状态码
     */
    private Integer code;
}
