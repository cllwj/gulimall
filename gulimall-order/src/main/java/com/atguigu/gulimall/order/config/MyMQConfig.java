package com.atguigu.gulimall.order.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.HashMap;
import java.util.Map;


/**
 * @author chenlei
 * @date 2021-11-24 9:50
 * @description
 */
@Slf4j
@Configuration
public class MyMQConfig {

    /**
     * 在spring里面可以直接使用@Bean
     * 容器中的这些组件使用@Bean会自动创建
     */
    /*@RabbitListener(queues = "order.release.order.queue")
    public void listener(OrderEntity entity, Channel channel, Message message) throws IOException {
        log.info("收到过期的叮当信息，准备关闭订单" + entity.getOrderSn());
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }*/

    /**
     * 死信队列/延时队列
     * @return
     */
    @Bean
    public Queue orderDelayQueue(){
        /**
         * x-dead-letter-exchange: order-event-exchange
         * x-dead-letter-routing-key: order.release.order
         * x-message-ttl: 60000
         */

        Map<String, Object> argumentsMap = new HashMap<>(3);
        argumentsMap.put("x-dead-letter-exchange","order-event-exchange");
        argumentsMap.put("x-dead-letter-routing-key","order.release.order");
        argumentsMap.put("x-message-ttl",60000);
        // String name, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
        return new Queue("order.delay.queue",true,false,false,argumentsMap);
    }

    /**
     * 普通队列
     * @return
     */
    @Bean
    public Queue orderReleaseOrderQueue(){
        return new Queue("order.release.order.queue",true,false,false);
    }

    @Bean
    public Exchange orderEventExchange(){
        // String name, boolean durable, boolean autoDelete, Map<String, Object> arguments
       return new TopicExchange("order-event-exchange",true,false);
    }

    @Bean
    public Binding orderCreateOrderBinding(){
        // String destination, Binding.DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
       return new Binding("order.delay.queue",
                    Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.create.order",
               null);
    }

    /**
     * 订单释放直接和库存释放进行绑定
     * @return
     */
    @Bean
    public Binding orderReleaseOrderBinding(){
        // String destination, Binding.DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
        return new Binding("order.release.order.queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.order",
                null);
         }

    @Bean
    public Binding orderReleaseOtherBinding(){
        // String destination, Binding.DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
        return new Binding("stock-release-stock-queue",
                Binding.DestinationType.QUEUE,
                "order-event-exchange",
                "order.release.other.#",
                null);
    }
}
