package com.atguigu.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author chenlei
 * @date 2021-11-16 17:19
 * @description
 */
@Data
public class FareVo {

    private MemberAddressVo memberAddressVo;
    private BigDecimal payPrice;
}
