package com.atguigu.gulimall.order.web;

import com.atguigu.gulimall.order.entity.OrderEntity;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

/**
 * @author chenlei
 * @date 2021-11-11 16:15
 * @description
 */
@Controller
public class HelloController {

    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * 测试下订单延时队列使用
     * @return
     */
    @ResponseBody
    @GetMapping("/test/createOrder")
    public String createOrderTest(){
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setModifyTime(new Date());
        String sn = UUID.randomUUID().toString();
        orderEntity.setOrderSn(sn);
        rabbitTemplate.convertAndSend("order-event-exchange","order.create.order",orderEntity);
        return "ok";
    }


    @GetMapping("/{page}.html")
    public String page(@PathVariable("page") String page){

        return page;
    }




}
