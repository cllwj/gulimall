package com.atguigu.gulimall.order.fegin;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.order.vo.WareSkuLockVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-11-16 9:40
 * @description
 */
@FeignClient("gulimall-ware")
public interface WareSkuFeginService {

    @PostMapping("/ware/waresku/hassotck")
    R getSkuHasStock(@RequestBody List<Long> skuIds);

    /**
     * 根据收货地址获取运费信息
     * @param addrId
     * @return
     */
    @GetMapping("/ware/wareinfo/fare")
    R getFare(@RequestParam("addrId") Long addrId);

    @PostMapping("/ware/waresku/lock/order")
    R orderLockStock(@RequestBody WareSkuLockVo vo);
}
