package com.atguigu.gulimall.order.web;

import com.atguigu.gulimall.order.service.OrderService;
import com.atguigu.gulimall.order.vo.OrderConfirmVo;
import com.atguigu.gulimall.order.vo.OrderSubmitVo;
import com.atguigu.gulimall.order.vo.SubmitOrderResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.jws.WebParam;
import java.util.concurrent.ExecutionException;

/**
 * @author chenlei
 * @date 2021-11-11 17:57
 * @description
 */
@Slf4j
@Controller
public class OrderWebController {

    @Autowired
    OrderService orderService;

    @GetMapping("/toTrade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = orderService.confirmOrder();
        model.addAttribute("orderConfirmData",confirmVo);
        return "confirm";
    }

    /**
     * 下单功能
     *
     * @param vo
     * @return
     */
    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo vo, Model model, RedirectAttributes redirectAttributes) {

        /**
         * 1、创建订单
         * 2、验证令牌
         * 3、验价格
         * 4、锁库存
         *
         * 下单成功来到支付选择页
         * 下单失败回到订单确认页重新确认订单信息
         */
        SubmitOrderResponseVo responseVo = orderService.submitOrder(vo);
        if (responseVo.getCode() == 0) {
            // 下单成功来到支付选择页
            model.addAttribute("submitOrderResp", responseVo);
            return "pay";
        } else {
            String msg = "商品下单失败;";
            switch (responseVo.getCode()) {
                case 1:
                    msg += "令牌验证失败";
                    break;
                case 2:
                    msg += "商品金额有所调整，请确认商品价格";
                    break;
                case 3:
                    msg += "商品库存不足";
                    break;
            }
            // redirectAttributes:这种数据传递方式是仿造session存储数据的方式
            redirectAttributes.addFlashAttribute("msg", msg);
            // 下单失败回到订单确认页重新确认订单信息
            return "redirect:http://order.gulimall.com/toTrade";
        }
    }
}
