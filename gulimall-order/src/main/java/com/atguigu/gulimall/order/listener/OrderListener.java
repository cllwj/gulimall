package com.atguigu.gulimall.order.listener;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.service.OrderService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author chenlei
 * @date 2021-11-24 20:22
 * @description
 */
@Slf4j
@Service
@RabbitListener(queues = "order.release.order.queue")
public class OrderListener {

    @Autowired
    OrderService orderService;

    @RabbitHandler
    public void handleStockedLocked(OrderEntity entity, Channel channel, Message message) throws IOException {
        log.info("收到MQ队列中的关单消息....");
        try {
            orderService.closeOrder(entity);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (Exception e) {
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
        }
    }
}
