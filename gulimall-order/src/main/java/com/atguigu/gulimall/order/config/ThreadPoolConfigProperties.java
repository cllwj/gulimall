package com.atguigu.gulimall.order.config;

import lombok.Data;

/**
 * @author chenlei
 * @date 2021-11-05 16:43
 * @description
 */
//@ConfigurationProperties(value = "gulimall.thread")
//@Component
@Data
public class ThreadPoolConfigProperties {

    private Integer coreSize;
    private Integer maxSize;
    private Integer keepAliveTime;
}
