package com.atguigu.gulimall.order.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.to.mq.OrderTo;
import com.atguigu.common.utils.R;
import com.atguigu.common.vo.MemberRespVo;
import com.atguigu.gulimall.order.constant.OrderConstant;
import com.atguigu.gulimall.order.entity.OrderItemEntity;
import com.atguigu.gulimall.order.enume.OrderStatusEnum;
import com.atguigu.gulimall.order.fegin.CartFeginService;
import com.atguigu.gulimall.order.fegin.MemberFeginService;
import com.atguigu.gulimall.order.fegin.ProductFeignService;
import com.atguigu.gulimall.order.fegin.WareSkuFeginService;
import com.atguigu.gulimall.order.interceptor.LoginUserInterceptor;
import com.atguigu.gulimall.order.service.OrderItemService;
import com.atguigu.gulimall.order.to.OrderCreateTo;
import com.atguigu.gulimall.order.vo.*;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.order.dao.OrderDao;
import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.service.OrderService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@Slf4j
@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {

    public ThreadLocal<OrderSubmitVo> orderSubmitLocal = new ThreadLocal<>();

    @Autowired
    private MemberFeginService memberFeginService;

    @Autowired
    private CartFeginService cartFeginService;

    @Autowired
    private ThreadPoolExecutor executor;

    @Autowired
    private WareSkuFeginService wareSkuFeginService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private ProductFeignService productFeignService;

    @Autowired
    private OrderItemService orderItemService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderEntity> page = this.page(
                new Query<OrderEntity>().getPage(params),
                new QueryWrapper<OrderEntity>()
        );

        return new PageUtils(page);
    }


    /**
     * 查询订单确认页需要的数据
     *
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Override
    public OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = new OrderConfirmVo();
        MemberRespVo memberRespVo = LoginUserInterceptor.loginUser.get();
        log.info("主线程：{}", Thread.currentThread().getId());

        // 获取之前的请求
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        // 收货地址列表 异步任务一
        CompletableFuture<Void> getAddressTask = CompletableFuture.runAsync(() -> {
            log.info("getAddress线程：{}", Thread.currentThread().getId());
            // 每一个线程都来共享之前的请求数据
            RequestContextHolder.setRequestAttributes(requestAttributes);
            List<MemberAddressVo> address = memberFeginService.getAddress(memberRespVo.getId());
            confirmVo.setAddress(address);
        }, executor);

        // 远程查询购物车中所有选中的购物项 异步任务二
        CompletableFuture<Void> getCartItemsTask = CompletableFuture.runAsync(() -> {
            log.info("getUserCartItems线程：{}", Thread.currentThread().getId());
            // 每一个线程都来共享之前的请求数据,在Feign的拦截器中就不会丢失老请求的数据
            RequestContextHolder.setRequestAttributes(requestAttributes);
            List<OrderItemVo> userCartItems = cartFeginService.getUserCartItems();
            confirmVo.setItems(userCartItems);
        }, executor).thenRunAsync(() -> {
            // 查询商品的库存信息
            List<OrderItemVo> items = confirmVo.getItems();
            if (items != null && !items.isEmpty()) {
                List<Long> collect = items.stream().map(item -> item.getSkuId()).collect(Collectors.toList());
                // 调用库存服务
                R hasStock = wareSkuFeginService.getSkuHasStock(collect);
                if (hasStock != null) {
                    List<SkuHasStockVo> data = hasStock.getData(new TypeReference<List<SkuHasStockVo>>() {
                    });
                    if (data != null && !data.isEmpty()) {
                        Map<Long, Boolean> hasStockMap = data.stream().collect(Collectors.toMap(SkuHasStockVo::getSkuId, SkuHasStockVo::getHasStock));
                        confirmVo.setStocks(hasStockMap);
                    }
                }
            }
        }, executor);

        // 用户积分
        Integer integration = memberRespVo.getIntegration();
        confirmVo.setIntegration(integration);

        // 防重令牌
        String token = UUID.randomUUID().toString().replace("-", "");
        confirmVo.setOrderToken(token);
        stringRedisTemplate.opsForValue().set(OrderConstant.USER_ORDER_TOKEN_PREFIX + memberRespVo.getId(), token, 30, TimeUnit.MINUTES);

        // 其他数据自动计算
        CompletableFuture.allOf(getAddressTask, getCartItemsTask).get();
        return confirmVo;
    }

    /**
     * 下单
     * @param vo
     * @return
     * @Transactional：本地事务，在分布式系统下，只能控制自己的回滚，控制不了其他服务的回滚，这里只能使用分布式事务处理
     * @GlobalTransactional:全局事务,使用seata解决分布式事务问题。由于seata处理的方式比较麻烦，后改为RabbitMQ的延迟队列的
     *                      方式处理分布式事务的问题。
     */
//    @GlobalTransactional
    @Transactional
    @Override
    public SubmitOrderResponseVo submitOrder(OrderSubmitVo vo) {
        SubmitOrderResponseVo response = new SubmitOrderResponseVo();
        response.setCode(0);
        MemberRespVo memberRespVo = LoginUserInterceptor.loginUser.get();
        orderSubmitLocal.set(vo);
        /**
         * 验证令牌的原因是保证接口的幂等性，即防止重复提交订单
         * 1、验证令牌【令牌的获取、令牌的对比和删除三个操作必须保证原子性】
         *     0:令牌失败
         *     1:删除成功
         *
         */
        String orderToken = vo.getOrderToken();
        // 使用redis的lua脚本来保证原子性
        String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
        // 原子验证令牌和删除令牌 【0 -》令牌验证失败，1-》删除成功】
        Long result = stringRedisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class),
                Arrays.asList(OrderConstant.USER_ORDER_TOKEN_PREFIX + memberRespVo.getId()), orderToken);

        if(result == 0L){
            // 令牌验证失败
            response.setCode(1);
            return response;
        }else{
            // 令牌验证成功，准备下单操作
            // 1、创建订单，订单项等信息
            OrderCreateTo order = createOrder();
            // 2、验价
            BigDecimal payAmount = order.getOrder().getPayAmount();
            BigDecimal payPrice = vo.getPayPrice();
            if(Math.abs(payPrice.subtract(payAmount).doubleValue()) <= 0.01){
                // 金额对比成功
                // 3、保存订单
                saveOrder(order);
                // 4、库存锁定，只要有异常需要回滚订单数据
                WareSkuLockVo wareSkuLockVo = new WareSkuLockVo();
                wareSkuLockVo.setOrderSn(order.getOrder().getOrderSn());
                List<OrderItemEntity> orderItems = order.getOrderItems();
                List<OrderItemVo> collect = orderItems.stream().map(item -> {
                    OrderItemVo orderItemVo = new OrderItemVo();
                    orderItemVo.setSkuId(item.getSkuId());
                    orderItemVo.setTitle(item.getSkuName());
                    orderItemVo.setCount(item.getSkuQuantity());
                    return orderItemVo;
                }).collect(Collectors.toList());
                wareSkuLockVo.setLocks(collect);
                //TODO 远程锁库存 ，库存成功了，但是网络原因超时了，订单回滚，库存不滚
                R r = wareSkuFeginService.orderLockStock(wareSkuLockVo);
                if(r.getCode() == 0){
                    response.setOrder(order.getOrder());
                    // TODO: 远程扣减积分 出现异常 如果使用传统的方式，那么订单回滚，锁定库存不回滚
                    // int i = 10/0;
                    // 将订单消息存入到延迟队列里面，一分钟后(真实情况下，是30分钟未支付，会自动取消订单)，如果状态为待支付，则执行关单操作
                    rabbitTemplate.convertAndSend("order-event-exchange","order.create.order",order.getOrder());
                    return response;
                }else{
                    // 商品库存不足
                    response.setCode(3);
                    return response;
                }
            }else{
                // 金额对比失败
                response.setCode(2);
                return response;
            }
        }
    }

    @Override
    public OrderEntity queryOrderBySn(String orderSn) {
        return baseMapper.selectOne(new QueryWrapper<OrderEntity>().eq("order_sn",orderSn));
    }

    /**
     * 业务逻辑：一分钟后，检查订单是否是未付款的状态，如果是，则要进行关单操作
     * @param entity
     */
    @Override
    public void closeOrder(OrderEntity entity) {
        // 查询订单的最新状态信息
        OrderEntity orderEntity = this.getById(entity.getId());
        if(orderEntity != null &&
                Objects.equals(orderEntity.getStatus(),OrderStatusEnum.CREATE_NEW.getCode())){
            // 未付款的状态，如果是，则要进行关单操作
            OrderEntity updateOrder = new OrderEntity();
            updateOrder.setId(entity.getId());
            updateOrder.setStatus(OrderStatusEnum.CANCLED.getCode());
            baseMapper.updateById(updateOrder);
            OrderTo orderTo = new OrderTo();
            BeanUtils.copyProperties(orderEntity,orderTo);
            // 向交换机再发送一个解锁库存的消息
            // TODO:保证消息一定会发送出去，每个消息都可以做好日志记录（给数据库保存每一个消息的详细信息）
            rabbitTemplate.convertAndSend("order-event-exchange","order.release.other",orderTo);
        }
    }

    @Override
    public PayVo getOrderPay(String orderSn) {
        PayVo payVo = new PayVo();
        OrderEntity orderEntity = queryOrderBySn(orderSn);
        BigDecimal bigDecimal = orderEntity.getPayAmount().setScale(2, BigDecimal.ROUND_UP);
        payVo.setTotal_amount(bigDecimal.toString());
        payVo.setOut_trade_no(orderSn);
        List<OrderItemEntity> orderItemEntities = orderItemService.list(new QueryWrapper<OrderItemEntity>().eq("order_sn", orderSn));
        if(orderItemEntities != null && !orderItemEntities.isEmpty()){
            OrderItemEntity orderItemEntity = orderItemEntities.get(0);
            payVo.setSubject(orderItemEntity.getSkuName());
            payVo.setBody(orderItemEntity.getSkuAttrsVals());
        }
        return payVo;
    }

    /**
     * 保存订单和订单项
     * @param order
     */
    private void saveOrder(OrderCreateTo order) {
        OrderEntity orderEntity = order.getOrder();
        orderEntity.setModifyTime(new Date());
        this.save(orderEntity);

        List<OrderItemEntity> orderItems = order.getOrderItems();
        orderItemService.saveBatch(orderItems);

    }

    private OrderCreateTo createOrder(){
        OrderCreateTo orderCreateTo = new OrderCreateTo();
        // 生成订单号
        String orderSn = IdWorker.getTimeId();
        // 1、构建订单
        OrderEntity orderEntity = bulidOrder(orderSn);
        // 2、获取到所有的订单项
        List<OrderItemEntity> orderItemEntities = buildOrderItems(orderSn);

        // 3、计算价格、积分信息
        computePrice(orderEntity,orderItemEntities);
        orderCreateTo.setOrder(orderEntity);
        orderCreateTo.setOrderItems(orderItemEntities);

        return orderCreateTo;
    }

    private void computePrice(OrderEntity orderEntity, List<OrderItemEntity> orderItemEntities) {
        BigDecimal total = new BigDecimal("0.0");
        BigDecimal integrationAmountTotal = new BigDecimal("0.0");
        BigDecimal promotionAmountTotal = new BigDecimal("0.0");
        BigDecimal couponAmountTotal = new BigDecimal("0.0");
        BigDecimal giftIntegrationTotal = new BigDecimal("0.0");
        BigDecimal giftGrowthTotal = new BigDecimal("0.0");

        for (OrderItemEntity orderItemEntity : orderItemEntities) {
            // 每项优惠金额累加
            integrationAmountTotal = integrationAmountTotal.add(orderItemEntity.getIntegrationAmount());
            promotionAmountTotal = promotionAmountTotal.add(orderItemEntity.getPromotionAmount());
            couponAmountTotal = couponAmountTotal.add(orderItemEntity.getCouponAmount());

            // 计算每项商品的总价，已经在构建订单项的时候已经计算了实际总金额
            BigDecimal realAmount = orderItemEntity.getRealAmount();
            // 累加每一项的实际总金额
            total = total.add(realAmount);

            // 订单项的积分信息
            giftIntegrationTotal = giftIntegrationTotal.add(new BigDecimal(orderItemEntity.getGiftIntegration().toString()));
            // 订单项的成长值信息
            giftGrowthTotal = giftGrowthTotal.add(new BigDecimal(orderItemEntity.getGiftIntegration().toString()));
        }
        // 订单价格相关
        orderEntity.setTotalAmount(total);
        // 订单应付总额 = 每个订单项的实际应付总额累加后的结果 + 订单运费
        orderEntity.setPayAmount(total.add(orderEntity.getFreightAmount()));
        orderEntity.setIntegrationAmount(integrationAmountTotal);
        orderEntity.setPromotionAmount(promotionAmountTotal);
        orderEntity.setCouponAmount(couponAmountTotal);

        // 设置积分等信息
        orderEntity.setGrowth(giftGrowthTotal.intValue());
        orderEntity.setIntegration(giftIntegrationTotal.intValue());
        orderEntity.setDeleteStatus(0); // 0：代表未删除
    }

    /**
     * 构建订单
     * @param orderSn
     */
    private OrderEntity bulidOrder(String orderSn) {
        OrderEntity orderEntity = new OrderEntity();
        MemberRespVo respVo = LoginUserInterceptor.loginUser.get();
        orderEntity.setMemberId(respVo.getId());
        orderEntity.setOrderSn(orderSn);
        // 获取订单的收货地址信息
        OrderSubmitVo orderSubmitVo = orderSubmitLocal.get();
        R fare = wareSkuFeginService.getFare(orderSubmitVo.getAddrId());
        FareVo fareResponse = fare.getData(new TypeReference<FareVo>() {
        });

        if(fareResponse != null){
            // 设置运费信息
            orderEntity.setFreightAmount(fareResponse.getPayPrice());
            // 设置收货人信息
            orderEntity.setReceiverCity(fareResponse.getMemberAddressVo().getCity());
            orderEntity.setReceiverDetailAddress(fareResponse.getMemberAddressVo().getDetailAddress());
            orderEntity.setReceiverName(fareResponse.getMemberAddressVo().getName());
            orderEntity.setReceiverPhone(fareResponse.getMemberAddressVo().getPhone());
            orderEntity.setReceiverPostCode(fareResponse.getMemberAddressVo().getPostCode());
            orderEntity.setReceiverProvince(fareResponse.getMemberAddressVo().getProvince());
            orderEntity.setReceiverRegion(fareResponse.getMemberAddressVo().getRegion());
            // 设置订单的相关状态信息
            orderEntity.setStatus(OrderStatusEnum.CREATE_NEW.getCode());
            orderEntity.setAutoConfirmDay(7);
        }
        return orderEntity;
    }

    /**
     * 构建所有订单项数据
     * @param orderSn
     * @return
     */
    private List<OrderItemEntity> buildOrderItems(String orderSn){
        List<OrderItemVo> userCartItems = cartFeginService.getUserCartItems();
        if(userCartItems != null && !userCartItems.isEmpty()){
            List<OrderItemEntity> orderItemEntities = userCartItems.stream().map(item -> {
                OrderItemEntity orderItemEntity = buildOrderItem(item);
                orderItemEntity.setOrderSn(orderSn);
                return orderItemEntity;
            }).collect(Collectors.toList());

            return orderItemEntities;
        }
        return null;
    }

    /**
     * 构建某一个订单项
     * @param item
     * @return
     */
    private OrderItemEntity buildOrderItem(OrderItemVo item) {
        OrderItemEntity orderItemEntity = new OrderItemEntity();
        // 远程调用获取订单项的spu信息
        R r = productFeignService.getSpuInfoBySkuId(item.getSkuId());
        SpuInfoVo spuInfo = r.getData(new TypeReference<SpuInfoVo>() {
        });
        // 设置SPU信息
        orderItemEntity.setSpuId(spuInfo.getId());
        orderItemEntity.setSpuName(spuInfo.getSpuName());
        orderItemEntity.setSpuBrand(spuInfo.getBrandId().toString());
        orderItemEntity.setCategoryId(spuInfo.getCatalogId());

        // 设置Sku信息
        orderItemEntity.setSkuId(item.getSkuId());
        orderItemEntity.setSkuName(item.getTitle());
        List<String> skuAttr = item.getSkuAttr();
        String skuAttrsVals = StringUtils.collectionToDelimitedString(skuAttr, ";");
        orderItemEntity.setSkuAttrsVals(skuAttrsVals);
        orderItemEntity.setSpuPic(item.getImage());
        orderItemEntity.setSkuPrice(item.getPrice());
        orderItemEntity.setSkuQuantity(item.getCount());

        //用户积分信息
        orderItemEntity.setGiftGrowth(item.getPrice().multiply(new BigDecimal(item.getCount().toString())).intValue());
        orderItemEntity.setGiftIntegration(item.getPrice().multiply(new BigDecimal(item.getCount().toString())).intValue());
        // 订单项的各种优惠信息
        orderItemEntity.setPromotionAmount(new BigDecimal("0"));
        orderItemEntity.setIntegrationAmount(new BigDecimal("0"));
        orderItemEntity.setCouponAmount(new BigDecimal("0"));
        // 订单项的原始总金额
        BigDecimal orign = orderItemEntity.getSkuPrice().multiply(new BigDecimal(orderItemEntity.getSkuQuantity().toString()));
        // 实际应付金额 = 原始总金额 - 各项优惠金额
        BigDecimal subtract = orign.subtract(orderItemEntity.getCouponAmount())
                .subtract(orderItemEntity.getIntegrationAmount())
                .subtract(orderItemEntity.getPromotionAmount());
        // 实际应付金额
        orderItemEntity.setRealAmount(subtract);

        return orderItemEntity;
    }


}