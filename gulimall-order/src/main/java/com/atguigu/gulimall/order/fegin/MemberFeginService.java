package com.atguigu.gulimall.order.fegin;

import com.atguigu.gulimall.order.vo.MemberAddressVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-11-12 10:24
 * @description 远程调用会员服务
 */
@FeignClient("gulimall-member")
public interface MemberFeginService {

    /**
     * 返回会员的所有收货地址列表
     * @param memberId
     * @return
     */
    @GetMapping("/member/memberreceiveaddress/{memberId}/address")
    List<MemberAddressVo> getAddress(@PathVariable("memberId") Long memberId);
}
