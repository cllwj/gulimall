package com.atguigu.gulimall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.atguigu.gulimall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2016101200672384";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCjulx4LruX3Q7oMbLv+WCDDX+xiAcwTpe6BvSuqSsj18CkKhXauARZGdVc5xtgtwg0ke1VvyXxR5KHHownkX2TGQtYpn/AoiBlGaD4g9p6XLb6NwqLjm/kbvuDaubCWwRFg/TCOqyQZW0yug448B4wfNjfgVH06/d/OlyBtpc7GTsMPZ7Nm3C5lsTEmR3HUt6rJOjNVhVrg3n+XsqOhFyAU1G3kw/BqP8mRapv+tPUcCSmcx1bgxJAt8daZlvAcmNfNVo3HyuAFCfsoxlakHARWRBQiuLZLrRLsl6CwGMCvFRnFvTY5kWTT9CFkgqyLtQkVsIPmE80W7774DeRRuDBAgMBAAECggEAS51W9V5H3vQF3XCOfkvCqePDFH+JKmMxqYVq3XtdZBlRa7/QBgo0X94Rbu2A9GKzlGvMm2xDHrJ6VpLiEagcwaGwXsnOeG2n3mk+sWdbsGU/UEG5SgiHXo1u9Rua5SnZyC0QDd/BMJLXIrVQujUTDDusgVSdQCr2S7jtXi6wm2bi2EGMpowNDxfGHaBVSh2E57W8ityXhx+4QqMmV7A+rnWKrFgDFkJqL1G4Odbx0LbeFVrqF1o1oYgbVx2btMdAJVM3kJDXwF+uYQR0YPmW7knSD0qkQ9gbRUDZiVGnLd31L0m7nZOWPzFSv7pAtq6D58uYyUXy3MCz85adT4rrXQKBgQD/on3FUjldiJ1UmnaiubeszOh6ocyWjTqj/1ruWS/DyoPzEhMVoq2IxSEF1kbDGtNJxY7YVwSjRs4yMePz1ESm3TRKlbD8Tx+rDjlWM1ABMdTeHGpW4XFM4lqFHfSdXbHN4/k7YtLlVtXW2crg3vCsZJi7bXNeV5gRQ4p5+JcDUwKBgQCj9kBWL83veMJiBPMDyETJgJDsmJXvIEODNtFfzNWr+us/EKhee2YzB7vcJe+/kH4JGZGywjJ0dZAfdDwcz8+KJMjq21batdgnxlwduh4em3CJYT60FrF10AGLgrGQlTkwJ0D8pkBF1QEVs1kR0DRUm59EetM1oykEXtGKwx59GwKBgQC83izWMJ25gUncTBMCvvv5a5sHn6ssQEPnDq78EmC741ABsoTvIzXbKunvChVV6OfaRV/4brI6gAGwDDRC3b2rGxubT2VnxH29P9EU4uxlNY/0WkIm1oGDMGl4mF02Nxc66wgk3Cq8zp9HEbpW0eA3PkupjoACX+02NBSHcScBiQKBgBrJeXtbNgbgsMk4y8cbafmNsp+k0iIOIFmIlC5VlbcSOGrGOuust2aw5cUywXeHkL/F2KLezUVvi4tH/yYp4VtNZTHRgeAbg2QEKTqGFLOla35WT9s2NensgMQdJwtbBcvTLe/Y7Xk7MoEsGuopHNOR1TRn6KfDr0jSgONtu0ZvAoGBAKlX1KXn/TfP6vmfn0DAateTPHzqvyJ+zNeT3kK4l4W2H91faXy4L+NWe2F5GPWmWhDtae04eEWoJZB0+W8B+9nbWgcifmopdSrzCLaYY+03YSnM8NATGf0A4y7Trv1T4KQZfT9fEznSaSR3ls2TKAfLh9YPg4TfbYOyrL+hrmjt";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAljovK1Z9nV+lmmTUkw/7I38sFRZ/QwKQDgV1w7n2WTk+cI+tIQ2x0pzr9QDfrt/R0J60n2RNNzMP6SmdOpsxQXL+e17+Sz2esj70genBkN+6Q6rdcQz6gzDFazR4Y73d5e16Fj8F08x+kb4TXwWA4YoFdG/QfePsboa54PG4Mbz6uTDmlyOytexAwcxS6Hmlhn4fDDIx8NOkBtiJTZ4PM1/Cva9ZjMjkiHUrw/70yyN/HN4VuTvs4S8ksxQBPqImQJCILPB0Vu5O7jYaXwoawbUzar42QIpcvlyksr8Q2nbmN0XW2Ih+WSSw0Jnlx13HpCV1WJF/CuPXR0HiGzFCgQIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url;

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url = "http://member.gulimall.com/memberOrder.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;

    }
}
