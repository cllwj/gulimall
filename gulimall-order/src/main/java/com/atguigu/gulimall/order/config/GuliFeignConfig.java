package com.atguigu.gulimall.order.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;

/**
 * @author chenlei
 * @date 2021-11-12 11:58
 * @description Feign远程调用拦截器
 */
@Slf4j
@Configuration
public class GuliFeignConfig {

    @Bean("requestInterceptor")
    public RequestInterceptor requestInterceptor(){
        return new RequestInterceptor() {
            @Override
            public void apply(RequestTemplate requestTemplate) {
                ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                // 老请求
                if(requestAttributes != null){
                    HttpServletRequest request = requestAttributes.getRequest();
                    log.info("RequestInterceptor线程：{}",Thread.currentThread().getId());
                    if(request != null){
                        // 同步请求头信息
                        String cookie = request.getHeader("Cookie");
                        // 新请求同步了老请求的cookie
                        requestTemplate.header("Cookie",cookie);
                    }
                }
            }
        };
    }
}
