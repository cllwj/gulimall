package com.atguigu.gulimall.order.fegin;

import com.atguigu.gulimall.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-11-12 11:04
 * @description
 */
@FeignClient("gulimall-cart")
public interface CartFeginService {

    @GetMapping("/getUserCartItems")
    @ResponseBody
    List<OrderItemVo> getUserCartItems();
}
