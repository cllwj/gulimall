package com.atguigu.gulimall.order.to;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author chenlei
 * @date 2021-11-16 22:35
 * @description
 */
@Data
public class OrderCreateTo {

    /**
     * 订单
     */
    private OrderEntity order;

    /**
     * 订单项信息
     */
    private List<OrderItemEntity> orderItems;

    /**
     * 订单计算的应付价格
     */
    private BigDecimal payPrice;

    /**
     * 运费
     */
    private BigDecimal fare;
}
