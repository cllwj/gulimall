package com.atguigu.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author chenlei
 * @date 2021-11-12 10:01
 * @description 订单确认页需要的数据
 */
@Data
public class OrderConfirmVo {

    /**
     * 收货地址 ums_member_receive_address表
     */
    List<MemberAddressVo> address;

    /**
     * 所有选中的购物项
     */
    List<OrderItemVo> items;

    /**
     * 优惠券信息
     */
    Integer integration;

    /**
     * 防重令牌，防止重复提交订单
     */
    String orderToken;

    /**
     * 是否有库存
     */
    Map<Long,Boolean> stocks;


    public int getCount(){
        int i = 0;
        if(items != null){
            for (OrderItemVo item : items) {
               i += item.getCount();
            }
        }
        return i;
    }

    /**
     * 订单总额
     */
    public BigDecimal getTotal() {
        BigDecimal sum = new BigDecimal("0");
        if(items != null){
            for (OrderItemVo item : items) {
                BigDecimal price = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));
                sum = sum.add(price);
            }
        }
        return sum;
    }

    /**
     * 应付价格
     */
    public BigDecimal getPayPrice() {
       return getTotal();
    }

}
