package com.atguigu.gulimall.order.constant;

/**
 * @author chenlei
 * @date 2021-11-16 15:48
 * @description
 */
public class OrderConstant {

    public static final String USER_ORDER_TOKEN_PREFIX = "order:token:";
}
