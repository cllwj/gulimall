package com.atguigu.gulimall.order.vo;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * @author chenlei
 * @date 2021-11-16 15:57
 * @description 封装订单提交的数据
 */
@ToString
@Data
public class OrderSubmitVo {

    /**
     * 收货地址ID
     */
    private Long addrId;

    /**
     * 支付方式
     */
    private Integer payType;

    /**
     * 防重令牌
     */
    private String orderToken;

    /**
     * 应付价格
     */
    private BigDecimal payPrice;

    // 用户相关信息，直接去session取出登录的用户信息

}
