package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-11-19 10:53
 * @description
 */
@Data
public class WareSkuLockVo {

    /**
     * 订单号
     */
    private String orderSn;

    /**
     * 要锁定的所有库存信息
     */
    private List<OrderItemVo> locks;
}
