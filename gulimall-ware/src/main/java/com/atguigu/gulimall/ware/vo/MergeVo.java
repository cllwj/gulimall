package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author chenlei
 * @date 2021-09-02 17:22
 * @description
 */
@Data
public class MergeVo {

    /**
     * 整单id
     */
    private Long purchaseId;

    /**
     * 合并项集合
     */
    private List<Long> items;

}
