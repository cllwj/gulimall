package com.atguigu.gulimall.ware.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author chenlei
 * @date 2021-11-24 16:13
 * @description
 */
@FeignClient("gulimall-order")
public interface OrderFeignService {

    @GetMapping("/order/order/queryOrderBySn/{orderSn}")
    R queryOrderBySn(@PathVariable("orderSn") String orderSn);
}
