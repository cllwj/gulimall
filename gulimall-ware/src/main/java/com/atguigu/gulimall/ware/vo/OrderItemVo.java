package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author chenlei
 * @date 2021-11-12 10:10
 * @description 选中后商品的信息
 */
@Data
public class OrderItemVo {

    private Long skuId;

    /**
     * 商品标题
     */
    private String title;

    /**
     * 商品图片
     */
    private String image;

    /**
     * 商品属性（可能有多种属性）
     */
    private List<String> skuAttr;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 商品数量
     */
    private Integer count;

    /**
     * 总价
     */
    private BigDecimal totalPrice;

    /**
     * 是否有货
     */
    private Boolean hasStock;
}
