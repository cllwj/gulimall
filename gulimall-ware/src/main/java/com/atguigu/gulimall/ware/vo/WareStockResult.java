package com.atguigu.gulimall.ware.vo;

import lombok.Data;

/**
 * @author chenlei
 * @date 2021-11-19 10:57
 * @description
 */
@Data
public class WareStockResult {


    /**
     * 锁定的是哪个商品的id
     */
    private Long skuId;

    /**
     * 该商品锁定的件数
     */
    private Integer num;

    /**
     * 该商品是否锁定库存
     */
    private Boolean lockd;
}
