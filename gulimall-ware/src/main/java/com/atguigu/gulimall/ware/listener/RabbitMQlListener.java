package com.atguigu.gulimall.ware.listener;

import com.atguigu.common.to.mq.OrderTo;
import com.atguigu.common.to.mq.StockLockedTo;
import com.atguigu.gulimall.ware.service.WareSkuService;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author chenlei
 * @date 2021-11-24 17:00
 * @description
 */
@Slf4j
@Service
@RabbitListener(queues = "stock-release-stock-queue")
public class RabbitMQlListener {

    @Autowired
    WareSkuService wareSkuService;

    /**
     * 解锁库存逻辑
     * @param stockLockedTo
     * @param message
     * @return
     *
     * 只要解锁库存的消息失败，一定要告诉服务器解锁失败
     */
    @RabbitHandler
    public void handleStockedLocked(StockLockedTo stockLockedTo, Message message, Channel channel) throws IOException {
        log.info("收到MQ队列中的解锁库存消息....");
        try {
            wareSkuService.unLockOrderStock(stockLockedTo);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (Exception e) {
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
        }
    }

    /**
     * 订单关闭，解锁库存
     * @param orderTo
     * @param message
     * @param channel
     */
    @RabbitHandler
    public void handleOrderCloseRelease(OrderTo orderTo,Message message,Channel channel) throws IOException {
        log.info("收到MQ队列中的解锁库存关闭订单的消息....");
        try {
            wareSkuService.unLockOrderStock(orderTo);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (Exception e) {
            channel.basicReject(message.getMessageProperties().getDeliveryTag(),true);
        }
    }
}
