package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author cl
 * @email sunlightcs@gmail.com
 * @date 2021-08-15 09:56:05
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
