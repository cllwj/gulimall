package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author cl
 * @email sunlightcs@gmail.com
 * @date 2021-08-15 09:56:05
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
