package com.atguigu.gulimall.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.exception.NoStockException;
import com.atguigu.common.to.mq.OrderTaskDetailTo;
import com.atguigu.common.to.mq.OrderTo;
import com.atguigu.common.to.mq.StockLockedTo;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.atguigu.gulimall.ware.entity.WareOrderTaskEntity;
import com.atguigu.gulimall.ware.feign.OrderFeignService;
import com.atguigu.gulimall.ware.service.WareOrderTaskDetailService;
import com.atguigu.gulimall.ware.service.WareOrderTaskService;
import com.atguigu.gulimall.ware.vo.OrderEntity;
import com.atguigu.gulimall.ware.vo.OrderItemVo;
import com.atguigu.gulimall.ware.vo.SkuHasStockVo;
import com.atguigu.gulimall.ware.vo.WareSkuLockVo;
import com.rabbitmq.client.Channel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.ware.dao.WareSkuDao;
import com.atguigu.gulimall.ware.entity.WareSkuEntity;
import com.atguigu.gulimall.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private WareOrderTaskService orderTaskService;

    @Autowired
    private WareOrderTaskDetailService wareOrderTaskDetailService;

    @Autowired
    private OrderFeignService orderFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        /**
         * skuId: 1
         * wareId: 1
         */
        QueryWrapper<WareSkuEntity> wrapper = new QueryWrapper<>();
        String skuId = (String) params.get("skuId");
        if(StringUtils.isNotBlank(skuId)){
            wrapper.eq("sku_id",skuId);
        }

        String wareId = (String) params.get("wareId");
        if(StringUtils.isNotBlank(wareId)){
            wrapper.eq("ware_id",wareId);
        }

        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds) {
        /**
         * select sum(stock - stock_locked) from wms_ware_sku where sku_id = 1
         */
        List<SkuHasStockVo> SkuHasStockVoList = skuIds.stream().map(skuId -> {
            SkuHasStockVo skuHasStockVo = new SkuHasStockVo();
            long count = baseMapper.getSkuStock(skuId);
            skuHasStockVo.setSkuId(skuId);
            skuHasStockVo.setHasStock(count > 0);
            return skuHasStockVo;
        }).collect(Collectors.toList());
        return SkuHasStockVoList;
    }


    @Override
    public void unLockOrderStock(StockLockedTo stockLockedTo) {
        OrderTaskDetailTo detailTo = stockLockedTo.getDetailTo();
        Long id = detailTo.getId();

        WareOrderTaskDetailEntity orderTaskDetailEntity = wareOrderTaskDetailService.getById(id);
        if (orderTaskDetailEntity != null) {
            // 查询订单状态判断是否取消，如果取消，则解锁库存，否则，不能取消
            Long lockedToId = stockLockedTo.getId();
            WareOrderTaskEntity taskEntity = orderTaskService.getById(lockedToId);
            String orderSn = taskEntity.getOrderSn();
            // 通过订单号远程查询订单信息
            R r = orderFeignService.queryOrderBySn(orderSn);
            if (r.getCode() == 0) {
                OrderEntity data = r.getData(new TypeReference<OrderEntity>() {
                });
                if (data == null || data.getStatus() == 4) {
                    // 订单已经被取消了，需要解锁库存
                    if (orderTaskDetailEntity.getLockStatus() == 1) {
                        unLockStock(detailTo.getSkuId(), detailTo.getWareId(), detailTo.getSkuNum(), id);
                        log.info("订单解锁成功....");
                    }
                }
            } else {
                // 消息拒绝以后重新放到队列里面，让别人继续消费解锁
                throw new RuntimeException("远程服务失败！");
            }
        } else {
            // 无需解锁
        }
    }

    /**
     * 防止订单服务卡顿，导致订单状态消息一直改不了，库存消息优先到期。查订单状态新建状态，什么都不做就走了
     * 导致卡顿的订单，永远不能解锁库存问题
     * @param orderTo
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void unLockOrderStock(OrderTo orderTo) {
        String orderSn = orderTo.getOrderSn();
        // 通过订单号查询工作单得到工作单id 查询最新库存状态，防止重复解锁库存
        WareOrderTaskEntity wareOrderTaskEntity = orderTaskService.queryOrderTaskBySn(orderSn);
        Long id = wareOrderTaskEntity.getId();
        // 通过工作单id查询详情单 找到所有没有解锁的库存，进行解锁
        List<WareOrderTaskDetailEntity> wareOrderTaskDetailEntityList = wareOrderTaskDetailService.list(
                new QueryWrapper<WareOrderTaskDetailEntity>()
                        .eq("task_id", id)
                        .eq("lock_status", 1));

        for (WareOrderTaskDetailEntity entity : wareOrderTaskDetailEntityList) {
            unLockStock(entity.getSkuId(),entity.getWareId(),entity.getSkuNum(),entity.getId());
        }
        log.info("补偿措施，解锁库存......");
    }

    /**
     * 解锁库存
     *
     */
    private void unLockStock(Long skuId,Long wareId,Integer num,Long detailId){
        baseMapper.unLockStock(skuId,wareId,num);

        // 库存工作单修改为已解锁状态
        WareOrderTaskDetailEntity entity = new WareOrderTaskDetailEntity();
        entity.setId(detailId);
        entity.setLockStatus(2);
        wareOrderTaskDetailService.updateById(entity);
    }

    /**
     * 商品锁定库存
     *
     * @param vo
     * @return
     * @Transactional:只要是运行时异常都会回滚 库存解锁的场景
     * 1）下单成功过
     */
    @Transactional(rollbackFor = NoStockException.class)
    @Override
    public Boolean orderLockStock(WareSkuLockVo vo) {
        /**
         * 保存工作单信息
         */
        WareOrderTaskEntity taskEntity = new WareOrderTaskEntity();
        taskEntity.setOrderSn(vo.getOrderSn());
        orderTaskService.save(taskEntity);

        List<OrderItemVo> locks = vo.getLocks();
        // 查询每件商品在库存表中对应的库存id
        List<SkuWareStockHas> stockHasList = locks.stream().map(item -> {
            SkuWareStockHas skuWareStockHas = new SkuWareStockHas();
            // 遍历每件商品的skuId查询库存表，得到对应的库存ID,一件商品可能存在于多个库存里面
            List<Long> wareIds = baseMapper.selectListWareIdBySkuId(item.getSkuId());
            skuWareStockHas.setSkuId(item.getSkuId());
            skuWareStockHas.setWareIds(wareIds);
            skuWareStockHas.setNum(item.getCount());
            return skuWareStockHas;
        }).collect(Collectors.toList());

        for (SkuWareStockHas skuWareStockHas : stockHasList) {
            Long skuId = skuWareStockHas.getSkuId();
            List<Long> wareIds = skuWareStockHas.getWareIds();
            Integer num = skuWareStockHas.getNum();
            if (wareIds == null || wareIds.isEmpty()) {
                // 没有任何仓库有该件商品
                throw new NoStockException(skuId);
            }

            boolean stock = false;
            for (Long wareId : wareIds) {
                // 通过商品ID和仓库id锁定库存信息
                int count = baseMapper.lockSkuStock(skuId, wareId, num);
                if (count == 1) {
                    // 如果返回值为1，说明该件商品锁定成功，直接跳出当前循环
                    stock = true;
                    /**
                     * 保存工作单详情信息
                     */
                    WareOrderTaskDetailEntity entity = new WareOrderTaskDetailEntity(null, skuId, "", num, taskEntity.getId(), wareId, 1);
                    wareOrderTaskDetailService.save(entity);

                    OrderTaskDetailTo orderTaskDetailTo = new OrderTaskDetailTo();
                    BeanUtils.copyProperties(entity, orderTaskDetailTo);
                    StockLockedTo stockLockedTo = new StockLockedTo();
                    stockLockedTo.setId(taskEntity.getId());
                    stockLockedTo.setDetailTo(orderTaskDetailTo);
                    /**
                     * 告诉MQ库存锁定成功
                     */
                    rabbitTemplate.convertAndSend("stock-event-exchange", "stock.locked", stockLockedTo);
                    break;
                }
            }
            if (!stock) {
                return false;
            }
        }
        return true;
    }



    @Data
    class SkuWareStockHas{
        private Long skuId;
        private Integer num;
        private List<Long> wareIds;
    }

}