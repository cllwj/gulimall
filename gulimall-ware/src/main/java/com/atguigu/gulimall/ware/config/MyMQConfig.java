package com.atguigu.gulimall.ware.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chenlei
 * @date 2021-11-24 14:10
 * @description
 */
@Configuration
public class MyMQConfig {

    /*@RabbitListener(queues = "stock-release-stock-queue")
    public void listener(Message message){

    }*/

    /**
     * 延时队列
     * @return
     */
    @Bean
    public Queue stockDelayQueue(){
        // String name, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
        /**
         * x-dead-letter-exchange: order-event-exchange
         * x-dead-letter-routing-key: order.release.order
         * x-message-ttl: 60000
         */
        Map<String,Object> args = new HashMap<>();
        args.put("x-dead-letter-exchange","stock-event-exchange");
        args.put("x-dead-letter-routing-key","stock.release");
        args.put("x-message-ttl",120000);
        return new Queue("stock-delay-queue",true,false,false,args);
    }

    @Bean
    public Queue stockReleaseStockQueue(){
       return new Queue("order.release.order.queue",true,false,false);
    }

    @Bean
    public Exchange stockEventExchange(){
        return new TopicExchange("stock-event-exchange",true,false);
    }

    @Bean
    public Binding stockLockedBinding(){
        // String destination, Binding.DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
       return new Binding("stock-delay-queue",
                Binding.DestinationType.QUEUE,
                "stock-event-exchange",
                "stock.locked",
                null);
    }

    @Bean
    public Binding stockReleaseBinding(){
        // String destination, Binding.DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
        return new Binding("stock-release-stock-queue",
                Binding.DestinationType.QUEUE,
                "stock-event-exchange",
                "stock.release.#",
                null);
    }

    @Bean
    public Binding orderReleaseOtherBinding(){
        // String destination, Binding.DestinationType destinationType, String exchange, String routingKey, Map<String, Object> arguments
        return new Binding("stock-release-stock-queue",
                Binding.DestinationType.QUEUE,
                "stock-event-exchange",
                "stock.release.#",
                null);
    }

}
